import pygame
from random import randint


def dibujar_tablero(surface, matriz_tablero, color):
    '''
    Funcion encargada de dibujar el tablero de batalla
    :param surface: surface de pygame para realizar el dibujo
    :param matriz_tablero: matriz del tablero para identificar donde dibujar los puntos
    :param color: ccolor para rellenar los puntos a dibujar
    :return:
    '''
    x_pos = 50
    y_pos = 100
    lista_rect = []
    background = pygame.image.load("sprites/ecenarios/battleground.jpg")
    surface.blit(background, (0, 0))
    for i in range(len(matriz_tablero)):
        for j in range(len(matriz_tablero[i])):
            rect = pygame.Rect(x_pos, y_pos, 25, 25)
            lista_rect.append(rect)
            x_pos += 100
        y_pos += 100
        x_pos = 50
    contador_rect = 0
    for y in range(len(matriz_tablero)):
        for x in range(len(matriz_tablero[y])):
            if matriz_tablero[y][x] == 0:
                pygame.draw.ellipse(surface, color, lista_rect[contador_rect], 0)
                pygame.display.update()
                contador_rect += 1
            else:
                contador_rect += 1
    return lista_rect


def crear_sprites_pokemon(pokemon_p1, pokemon_p2):
    '''
    Crea objetos Sprites que pygame utiliza para mostrar imagenes
    :param pokemon_p1: lista de pokemones del jugador 1 a cargar la sprite
    :param pokemon_p2: lista de pokemones del jugador 2 a cargar las sprite
    :return:
    '''
    lista_p1 = []
    lista_p2 = []
    for poke_numero in pokemon_p1:
        pokemon = pygame.image.load("sprites/pokemon/tablero/front/" + str(poke_numero) + ".png")
        lista_p1.append(pokemon)
    for poke_numero in pokemon_p2:
        pokemon = pygame.image.load("sprites/pokemon/tablero/front/" + str(poke_numero) + ".png")
        lista_p2.append(pokemon)
    return (lista_p1, lista_p2)


def sprite_pokemon(matriz_tablero, pokemon_dibujar, posicion, surface, lista_sprite):
    '''
    funcion para crear areas de contacto para poder identificar correctamente si el mouse choca con alguna sprite
    :param matriz_tablero: matriz del tablero
    :param pokemon_dibujar: sprite de pokemon que se desea dibujar en pantalla
    :param posicion: posicion en la cual se dibuja el pokemon y se establece el area de contacto
    :param surface: Surface de pygame en donde se desea dibujar el pokemon
    :param lista_sprite: lista vacia en donde se almacenaran todas las sprites con sus respectivas areas
    :return:
    '''
    sprite_pokemon = pygame.sprite.Sprite()
    sprite_pokemon.image = pokemon_dibujar
    sprite_pokemon.rect = pokemon_dibujar.get_rect()
    sprite_pokemon.rect.left = posicion[0]
    sprite_pokemon.rect.top = posicion[1]
    surface.blit(sprite_pokemon.image, posicion)
    lista_sprite.append(sprite_pokemon)


def dibujar_pokemon(surface, lista_sprites, posicion, seleccionado):
    '''
    Verifica si el pokemon a sido seleccionado y de ser asi lo dibuja en la posicion asignada, de lo contrario solo lo redibuja son moverlo
    :param surface: Surface de pygame en la cual se dibujara
    :param lista_sprites: lista que contiene todas las sprites del jugador
    :param posicion: posicion nueva a dibujar el pokemon en caso de que sea el seleccionado
    :param seleccionado: indice de pokemon seleccionado para dibujar
    :return:
    '''
    tablero_x = posicion[0] // 100
    tablero_y = posicion[1] // 100
    if 0 <= tablero_x <= 6 and 0 <= tablero_y <= 5:
        for sprite in range(len(lista_sprites)):
            if sprite == seleccionado:
                surface.blit(lista_sprites[sprite].image, posicion)
            else:
                surface.blit(lista_sprites[sprite].image,
                             (lista_sprites[sprite].rect.left, lista_sprites[sprite].rect.top))


def seleccionar_tablero(lista_sprites, seleccionado, matriz_tablero, mouse_x, mouse_y, lista_rect, surface,
                        lista_oponente, primer_movimiento, identificador):
    '''
    Funcion que determina si se selecciona un posicion valida del tablero para dibujar
    :param lista_sprites: lista con todas las sprites del jugador
    :param seleccionado: indice del pokemon que ha sido seleccionado
    :param matriz_tablero: matriz del tablero para verificar si se puede dibujar y de ser asi cambiar el valor de la posicion en la matriz
    :param mouse_x: posicion x del mouse
    :param mouse_y: posicion y del mouse
    :param lista_rect: lista con las posiciones de todos los lugares a dibujar del tablero
    :param surface: surface de pygame en la cual dibujar
    :param lista_oponente: lista con todas las sprites del oponente
    :param primer_movimiento: define si el pokemon esta en la banca o no
    :param identificador: define si el que realiza el movimiento es el host o el cliente
    :return: retorna True en caso de encontrarse en una posicion permitida para dibujar
    '''
    for rectangulo in lista_rect:
        if primer_movimiento:
            if rectangulo.collidepoint(mouse_x, mouse_y):
                x_anterior = (lista_sprites[seleccionado].rect.left // 100)
                y_anterior = (lista_sprites[seleccionado].rect.top // 100)
                x = (mouse_x // 100)
                y = (mouse_y // 100) - 1
                if 0 <= x_anterior <= 6 and 0 <= y_anterior <= 5:
                    matriz_tablero[y_anterior][x_anterior] = 0
                if identificador == "host":
                    matriz_tablero[y][x] = 2
                if identificador == "client":
                    matriz_tablero[y][x] = 1
                lista_posiciones = dibujar_tablero(surface, matriz_tablero, (184, 184, 184))
                (lista_sprites[seleccionado].rect.left, lista_sprites[seleccionado].rect.top) = (
                rectangulo.left - 35, rectangulo.top - 35)
                dibujar_pokemon(surface, lista_sprites, (rectangulo.left - 35, rectangulo.top - 35), seleccionado)
                for pokemon_oponente in lista_oponente:
                    dibujar_pokemon(surface, lista_oponente, (pokemon_oponente.rect.left, pokemon_oponente.rect.left),
                                    -1)
                print(matriz_tablero)
                return True
        else:
            if rectangulo.collidepoint(mouse_x, mouse_y):
                x_anterior = (lista_sprites[seleccionado].rect.left // 100)
                y_anterior = (lista_sprites[seleccionado].rect.top // 100)
                x = (mouse_x // 100)
                y = (mouse_y // 100) - 1
                if (x == x_anterior + 1 and y == y_anterior) or (x == x_anterior - 1 and y == y_anterior) or (
                        x == x_anterior and y == y_anterior + 1) or (x == x_anterior and y == y_anterior - 1):
                    matriz_tablero[y_anterior][x_anterior] = 0
                    if identificador == "host":
                        matriz_tablero[y][x] = 2
                    if identificador == "client":
                        matriz_tablero[y][x] = 1
                    lista_posiciones = dibujar_tablero(surface, matriz_tablero, (184, 184, 184))
                    (lista_sprites[seleccionado].rect.left, lista_sprites[seleccionado].rect.top) = (
                    rectangulo.left - 35, rectangulo.top - 35)
                    dibujar_pokemon(surface, lista_sprites, (rectangulo.left - 35, rectangulo.top - 35), seleccionado)
                    for pokemon_oponente in lista_oponente:
                        dibujar_pokemon(surface, lista_oponente,
                                        (pokemon_oponente.rect.left, pokemon_oponente.rect.left), -1)
                    print(matriz_tablero)
                    return True


def detectar_pelea(matriz_pelea):
    '''
    Detecta si hay dos pokemones continuos para realizar la pelea
    :param matriz_pelea: matriz de posiciones del tablero
    :return: retorna si encontro o no una pelea
    '''
    pelea_encontrada = False
    for y in range(len(matriz_pelea)):
        for x in range(1, len(matriz_pelea[y])):
            if (matriz_pelea[y][x - 1] == 1 and matriz_pelea[y][x] == 2) or (
                    matriz_pelea[y][x - 1] == 2 and matriz_pelea[y][x] == 1):
                pelea_encontrada = True
                break
    for x in range(len(matriz_pelea[0])):
        for y in range(1, len(matriz_pelea)):
            if (matriz_pelea[y - 1][x] == 1 and matriz_pelea[y][x] == 2) or (
                    matriz_pelea[y - 1][x] == 2 and matriz_pelea[y][x] == 1):
                pelea_encontrada = True
                break
    return pelea_encontrada


def dibujar_pokemon_pelea(surface, lista_p1, lista_p2, poke_team_p1, poke_team_p2, dibujar_p1, dibujar_p2):
    '''
    Dibuja la pantalla de pelea en el tablero
    :param surface: surface de pygame en la que se dibujara la pantalla de pelea
    :param lista_p1: lista de las sprites del jugador 1
    :param lista_p2: lista de las sprites del jugador 2
    :param poke_team_p1: lista de pokemones en el equipo del jugador
    :param poke_team_p2: lista de pokemones en el equipo del oponente
    :param dibujar_p1: pokemon a dibujar del jugador 1
    :param dibujar_p2: pokemon a dibujar del juador 2
    :return:
    '''
    indice_p1 = 0
    indice_p2 = 0
    for i in range(len(poke_team_p1)):
        if poke_team_p1[i] == dibujar_p1:
            indice_p1 = i
            break
    for i in range(len(poke_team_p2)):
        if poke_team_p2[i] == dibujar_p2:
            indice_p2 = i
            break
    surface.blit(lista_p2[indice_p2].image, (186, 143))
    surface.blit(lista_p1[indice_p1].image, (456, 543))


def pelea_pokemones(surface, lista_sprite_jugador, lista_sprite_oponente, team_p1, team_p2, indice_1, indice_2,
                    numero_1, numero_2):
    '''
    Esta funcion se encarga de realizar todas las acciones relacionadas con la pelea de 2 pokemones
    :param surface: surface de pygame en donde dibujar
    :param lista_sprite_jugador: lista que contiene todos los sprites del jugador 1
    :param lista_sprite_oponente: lista que contiene todos los sprites del jugador 2
    :param team_p1: lista con los pokemones en el equipo del jugador
    :param team_p2: lista con los pokemones en el equipo del oponente
    :param indice_1: indice del pokemon del jugador 1 a dibujar
    :param indice_2: indice del pokemon del jugador 2 a dibujar
    :param numero_1: numero generado para la lucha (pertenece al jugador 1)
    :param numero_2:numero generado para la lucha (pertenece al jugador 2)
    :return:
    '''
    dibujar_pantalla_pelea(surface)
    dibujar_pokemon_pelea(surface, lista_sprite_jugador, lista_sprite_oponente, team_p1, team_p2, indice_1, indice_2)
    Fuente_pokemon = pygame.font.SysFont("Pokemon R/B/Y", 40)
    Numero_1 = Fuente_pokemon.render(str(numero_1), 1, (0, 0, 0))
    surface.blit(Numero_1, (325, 400))
    Numero_2 = Fuente_pokemon.render(str(numero_2), 1, (0, 0, 0))
    surface.blit(Numero_2, (368, 309))


def dibujar_pantalla_pelea(surface):
    '''
    Dibuja la imagen de la pantalla de pelea
    :param surface: Surface de python a realizar el trazo
    :return:
    '''
    background = pygame.image.load("sprites/ecenarios/batalla_duel.jpg")
    surface.blit(background, (150, 94))


def comienza_juego(surface, matriz_tablero, lista_sprites_p1, lista_sprites_p2, mensaje, lista_p1, lista_p2):
    '''
    Funcioin que se encarga de inicializar todos los parametros para el tablero
    :param surface: pantalla en la cual dibujar
    :param matriz_tablero: matriz del tablero
    :param lista_sprites_p1: lista que contiene las sprites del jugador 1
    :param lista_sprites_p2: lista que contiene las sprites del jugador 2
    :param mensaje: mensaje a ser enviado por el cliente
    :param lista_p1: lista de pokemones del jugador 1
    :param lista_p2: lista de pokemones del jugador 2
    :return:
    '''
    salir = False
    reloj1 = pygame.time.Clock()
    blanco = (255, 255, 255)
    gris = (184, 184, 184)
    lista_posiciones = dibujar_tablero(surface, matriz_tablero, gris)
    for pokemon in range(len(lista_sprites_p1)):
        sprite_pokemon(matriz_tablero, lista_sprites_p1[pokemon],
                       mensaje["jugador1"]["pokemones_banca"][pokemon]["posicion"], surface, lista_p1)
    for pokemon in range(len(lista_sprites_p1)):
        sprite_pokemon(matriz_tablero, lista_sprites_p2[pokemon],
                       mensaje["jugador2"]["pokemones_banca"][pokemon]["posicion"], surface, lista_p2)
    pygame.display.update()
    return lista_posiciones


def play_music():
    '''
    Funcion que oomienza a a sonar la musica
    :return:
    '''
    music = "music/battle_tablero.mp3"
    pygame.mixer.music.load(music)
    pygame.mixer.music.play(-1)


def iniciar():
    '''
    Funcion para correr el tablero en modo offline
    :return:
    '''
    pygame.init()
    pygame.font.init()
    pygame.mixer.init()
    pantalla = pygame.display.set_mode((700, 700))
    pygame.display.set_caption("Pokemon Duel")
    salir = False
    reloj1 = pygame.time.Clock()
    blanco = (255, 255, 255)
    gris = (184, 184, 184)
    matriz_tablero = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0]]
    poke_team = [151, 150, 25]
    poke_team_2 = [65, 52, 69]
    lista_sprites_p1 = []
    lista_sprites_p2 = []
    lista_sprites = []
    lista_oponente = []
    primer_movimiento = True
    mi_turno = True
    pantalla_pelea = False
    identificador = "client"
    pantalla.fill(blanco)
    lista_posiciones = dibujar_tablero(pantalla, matriz_tablero, gris)
    (lista_p1, lista_p2) = crear_sprites_pokemon(poke_team, poke_team_2)
    pelea = False
    x_inicial = 250
    seleccionado = -1
    for pokemon in lista_p1:
        sprite_pokemon(matriz_tablero, pokemon, (x_inicial, 16), pantalla, lista_sprites_p1)
        x_inicial += 100
    x_inicial = 250
    # (186, 143)
    for pokemon in lista_p2:
        sprite_pokemon(matriz_tablero, pokemon, (x_inicial, 600), pantalla, lista_sprites_p2)
        x_inicial += 100
    if identificador == "host":
        lista_sprites = lista_sprites_p1
        lista_oponente = lista_sprites_p2
    if identificador == "client":
        lista_sprites = lista_sprites_p2
        lista_oponente = lista_sprites_p1
    play_music()
    while salir != True:
        # BORRAR SOLO PARA PRUEBAS
        if mi_turno:
            identificador = "client"
        if mi_turno == False:
            identificador = "host"
        if identificador == "host":
            lista_sprites = lista_sprites_p1
            lista_oponente = lista_sprites_p2
        if identificador == "client":
            lista_sprites = lista_sprites_p2
            lista_oponente = lista_sprites_p1
        # BORRAR SOLO PARA PRUEBAS
        (mouse_x, mouse_y) = pygame.mouse.get_pos()
        print((mouse_x, mouse_y))
        for event in pygame.event.get():
            if pantalla_pelea and (event.type == pygame.MOUSEBUTTONDOWN):
                pantalla_pelea = False
                dibujar_tablero(pantalla, matriz_tablero, gris)
                for i in range(len(lista_sprites)):
                    dibujar_pokemon(pantalla, lista_sprites, (lista_sprites[i].rect.left, lista_sprites[i].rect.top),
                                    -1)
                for i in range(len(lista_oponente)):
                    dibujar_pokemon(pantalla, lista_oponente, (lista_sprites[i].rect.left, lista_sprites[i].rect.top),
                                    -1)
                    matriz_tablero[lista_sprites[i].rect.top // 100][lista_sprites[i].rect.left // 100] = 0
            if seleccionado != -1 and event.type == pygame.MOUSEBUTTONDOWN:
                if seleccionar_tablero(lista_sprites, seleccionado, matriz_tablero, mouse_x, mouse_y, lista_posiciones,
                                       pantalla, lista_oponente, primer_movimiento, identificador):
                    primer_movimiento = False
                    seleccionado = -1
                    if detectar_pelea(matriz_tablero):
                        pantalla_pelea = True
                        random_1 = randint(0, 100)
                        random_2 = randint(0, 100)
                        pelea_pokemones(pantalla, lista_sprites, lista_oponente, poke_team, poke_team_2,
                                        poke_team[seleccionado + 1], poke_team_2[seleccionado + 1], random_1, random_2)
                        if random_1 > random_2:
                            matriz_tablero[lista_oponente[seleccionado + 1].rect.top // 100][
                                lista_oponente[seleccionado + 1].rect.left // 100] = 0
                            lista_oponente[seleccionado + 1].rect.left = 250
                            lista_oponente[seleccionado + 1].rect.top = 16
                        else:
                            matriz_tablero[lista_sprites[seleccionado + 1].rect.top // 100][
                                lista_sprites[seleccionado + 1].rect.left // 100] = 0
                            lista_sprites[seleccionado + 1].rect.left = 250
                            lista_sprites[seleccionado + 1].rect.top = 600
                    if mi_turno == False:
                        mi_turno = True
                    elif mi_turno:
                        mi_turno = False
            if event.type == pygame.QUIT:
                salir = True
            if event.type == pygame.MOUSEBUTTONDOWN:
                for sprite in range(len(lista_sprites)):
                    if lista_sprites[sprite].rect.collidepoint(mouse_x, mouse_y):
                        if identificador == "host":
                            if lista_sprites[sprite].rect.top == 16:
                                primer_movimiento = True
                        if identificador == "client":
                            if lista_sprites[sprite].rect.top == 600:
                                primer_movimiento = True
                        seleccionado = sprite
        pygame.display.update()

