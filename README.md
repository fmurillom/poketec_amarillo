# PokeTec_Amarillo #

Bienvenido al Git para el PokeTec_Amarillo el cual es nuestro primer projecto programado del curso [CE-1102] Taller de Programacion del instittuto tecnologico de Costa Rica

### Setup ###
Este codigo fue realizado con el interpretador Python en su version 3.6

Para el correcto uso de este codigo la estructura de la carpeta contenedora debe ser la siguiente:
install\
music\
save\
sprites\
PokeTec.py

Una vez que se tenga la estructura de carpetas por favor ingrese a la carpeta install y asegurese de instalar la fuente Pokefont.ttf contenida en esta carpeta.

Una vez realizado esto asegurese de tener el modulo pygame instalado, ya que sin el el codigo no correra.

Para instalar pygame basta con abrir un cmd en modo de administrador, y ejecutar el comando:
pip install pygame

###Autores##
Los autores de este codigo son:
Francisco J. Murillo Morgan y Anthony Villegas