import tornado.web
from tornado.ioloop import IOLoop
from src.server_functions import *
import pyautogui

class Mainhandler(tornado.web.RequestHandler):
    '''
    Handler encargado de imprimir en pantalla al acceder a la direccion principal
    '''
    def get(self):
        self.write("Bienvenido al servidor PokeDex")

class PokemonHandler(tornado.web.RequestHandler):
    '''
    Handler que guarda la imagenes recibidas
    '''
    def post(self):
        comando = self.get_argument("comando")
        if comando == "upload":
            try:
                pokeon_name = self.get_argument("pokemon")
                img_base64 = self.get_argument("photo")
                pokedex_list(lista_pokedex, pokeon_name)
                save_log(lista_pokedex)
                with open("Pokemon/{}.jpg".format(pokeon_name), "wb") as file_writer:
                    file_writer.write(base64.b64decode(img_base64))
                self.write("true")
            except Exception as ex:
                print(ex)
                self.write("false")

class PokemonHandler2(tornado.web.RequestHandler):
    '''
    Handler que envia el nombre de los pokemones disponibles y que tambien recibe el indice del pokemon sleccionado
    '''
    def get(self):
        dex_entry = crear_string_pokemon(lista_pokedex)
        self.write(dex_entry)

    def post(self):
        global indice
        try:
            indice = self.get_argument("indice")
            self.write("true")
        except Exception as ex:
            print(ex)
            self.write("false")

class ImageHandler(tornado.web.RequestHandler):
    '''
    Envia el string de la imagen convertida
    '''
    def get(self):
        lista_base64 = convertir_base64(lista_pokedex)
        self.write(lista_base64[int(indice)])


class ControlHandler(tornado.web.RequestHandler):
    '''
    Handler que controla los datos recibidos del servidor
    '''
    def post(self):
        try:
            action = self.get_argument("action")
            pyautogui.press(action)
            self.write("true")
        except Exception as ex:
            print(ex)
            self.write("false")

class Application(tornado.web.Application):
    '''
    Aplicacion de control de todos los handlers con sus respectivos links a ser accedidos
    '''
    def __init__(self):
        handlers = [
            (r"/?", Mainhandler),
            (r"/api/v1/pokemon", PokemonHandler),
            (r"/api/v1/control/?", ControlHandler),
            (r"/api/v1/pokemon/Dex", PokemonHandler2),
            (r"/api/v1/pokemon/Dex_image", ImageHandler)
            ]
        tornado.web.Application.__init__(self, handlers)

def main_server():
    '''
    Inicia el main del servidor
    :return:
    '''
    global lista_pokedex
    global indice
    lista_pokedex = load_log()
    app = Application()
    app.listen(8080)
    IOLoop.instance().start()

if __name__ == "__main__":
    main_server()