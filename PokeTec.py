import time

import random
import socket
import pickle

from Tablero_Batalla import *
from src.Entrenador import *
from src.Manejo_Grafico import *
from src.Wild_Pokemon import *
from Tablero_Batalla import *
import threading


# Verifica si las nuevas coordenadas del personage al moverse chocaran con algun pokemon salvage, si choca retorna False,
# si no entonces retorna True
# Entradas: new_coordinates son las coordenadas nuevas del personaje antes de realizar el movimiento, poke_lista contiene
# las coordenadas de los pokemones salvages
def choca_wild_pokemon(new_coordniates, poke_list):
    global poke_number, wild_pokemon_battle_1, selected_pokemon, capturado
    selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[0][0]) + ".png")
    if new_coordniates[0] == poke_list[0][1] and new_coordniates[1] == poke_list[0][2]:
        wild_pokemon_battle_1 = PhotoImage(file="sprites/pokemon/front/" + str(poke_list[0][0]) + ".png")
        poke_number = poke_list[0][0]
        capturado = 1
        return False
    elif new_coordniates[0] == poke_list[1][1] and new_coordniates[1] == poke_list[1][2]:
        wild_pokemon_battle_1 = PhotoImage(file="sprites/pokemon/front/" + str(poke_list[1][0]) + ".png")
        poke_number = poke_list[1][0]
        capturado = 2
        return False
    elif new_coordniates[0] == poke_list[2][1] and new_coordniates[1] == poke_list[2][2]:
        wild_pokemon_battle_1 = PhotoImage(file="sprites/pokemon/front/" + str(poke_list[2][0]) + ".png")
        poke_number = poke_list[2][0]
        capturado = 3
        return False
    elif new_coordniates[0] == poke_list[3][1] and new_coordniates[1] == poke_list[3][2]:
        wild_pokemon_battle_1 = PhotoImage(file="sprites/pokemon/front/" + str(poke_list[3][0]) + ".png")
        poke_number = poke_list[3][0]
        capturado = 4
        return False
    elif new_coordniates[0] == poke_list[4][1] and new_coordniates[1] == poke_list[4][2]:
        wild_pokemon_battle_1 = PhotoImage(file="sprites/pokemon/front/" + str(poke_list[4][0]) + ".png")
        poke_number = poke_list[4][0]
        capturado = 5
        return False
    else:
        return True


# Define si el entrenador esta en frente de un pokemon y de ser asi retorna True para abrir la interfaz de batalla
def entrar_pelea():
    coordinates_red = ventana_principal.coords("Red")
    if choca_wild_pokemon([coordinates_red[0], coordinates_red[1] - 25], poke_list) == False:
        return True
    elif choca_wild_pokemon([coordinates_red[0], coordinates_red[1] + 25], poke_list) == False:
        return True
    elif choca_wild_pokemon([coordinates_red[0] + 25, coordinates_red[1]], poke_list) == False:
        return True
    elif choca_wild_pokemon([coordinates_red[0] - 25, coordinates_red[1]], poke_list) == False:
        return True
    else:
        return False


# Primera interfaz de batalla, cambia el fondo y elimina las sprites del jugador y el pikachu, cambia la musica a musica
# de batalla
def pelea_1():
    global nombre_poke_wild, music, vida_wild, vida_wild_fija, vida_pokemon_fija, poke_team, back_ident
    vida_wild = vida_wild_fija
    vida_pokemon_fija = poke_team[pokemon_en_batalla][1]
    delete_wild_pokemon(ventana_principal)
    ventana_principal.delete("Red")
    ventana_principal.delete("pikachu")
    back_ident = cambiar_background("battle_1", ventana_principal, background_sprite, background)
    nombre_poke_wild = Label(ventana_principal, text=nombres_pokemon[poke_number - 1], font=("Pokemon R/B/Y", 25))
    nombre_poke_wild.place(x=170, y=460)
    music = "music/battle.mp3"
    pygame.mixer.music.stop()
    pygame.mixer.music.load(music)
    pygame.mixer.music.play(-1)


# Segunda interfaz de batalla, en esta se puede hacer la seleccion de ataque, capturar o cambiar de pokemon para batallar
# Entradas: pokemon_usado sera el numero de indice de pokemon a utilizar en la batalla segun la lista de pokemones en el
# equipo
def pelea_2(pokemon_usado):
    global ventana_principal, nombre_poke_red, nombre_poke_wild, vida_enemigo, vida_pokemon, vida_wild, vida_wild_fija, \
        vida_pokemon_fija, back_ident
    nombre_poke_wild.destroy()
    ventana_principal.delete("Red")
    ventana_principal.delete("pikachu")
    delete_wild_pokemon(ventana_principal)
    back_ident = cambiar_background("battle_2", ventana_principal, background_sprite, background)
    wild_pokemon_1 = ventana_principal.create_image((450, 150), image=wild_pokemon_battle_1,
                                                    tag="wild_poke")
    selected_pokemon_1 = ventana_principal.create_image((150, 300), image=selected_pokemon, tag="pokemon_seleccionado")

    flecha_seleccion_1 = ventana_principal.create_image((280, 485), image=flecha_seleccion, tag="flecha")
    nombre_poke_wild = Label(ventana_principal, text=nombres_pokemon[poke_number - 1], font=("Pokemon R/B/Y", 25))
    vida_enemigo = Label(ventana_principal, text=str(vida_wild) + "/" + str(vida_wild_fija), font=("Pokemon R/B/Y", 15))
    vida_pokemon = Label(ventana_principal, text=str(poke_team[pokemon_en_batalla][1]) + "/"
                                                 + str(poke_team[pokemon_en_batalla][2]), font=("Pokemon R/B/Y", 15))
    nombre_poke_wild.configure(font=("Pokemon R/B/Y", 15), bg="white")
    nombre_poke_wild.place(x=50, y=50)
    vida_enemigo.place(x=125, y=85)
    nombre_poke_red = Label(ventana_principal, text=nombres_pokemon[int(poke_team[pokemon_usado][0]) - 1],
                            font=("Pokemon R/B/Y", 15), bg="white")
    nombre_poke_red.place(x=385, y=285)
    vida_pokemon.place(x=385, y=325)


# Al seleccionar la opcion de Items, esta abre una nueva ventana de juego para mostrar las pokebolas, y permite
# seleccionarlas
def items():
    global ventana_principal, nombre_poke_red, item_menu, pokebola, cantidad_pokebola, vida_pokemon
    item_menu = True
    nombre_poke_red.destroy()
    ventana_principal.delete("flecha")
    item_box_show = ventana_principal.create_image((300, 300), image=item_box, tag="itembox")
    pokebola = Label(ventana_principal, text="Pokeball", font=("Pokemon R/B/Y", 15))
    cantidad_pokebola = Label(ventana_principal, text="999", font=("Pokemon R/B/Y", 15))
    pokebola.place(x=200, y=250)
    cantidad_pokebola.place(x=370, y=250)
    flecha_2 = ventana_principal.create_image((190, 265), image=flecha_seleccion, tag="flecha_2")
    vida_pokemon.destroy()


def servidor():
    global ventana_principal, servidor_estado
    servidor_estado = True
    obtener_ip_port()
    socket_server()


def cliente():
    '''
    Funcion, utilizada para hacer un llamado formal de la fucion cliente de los socket
    :return:
    '''
    global cliente_estado
    cliente_estado = True
    socket_client()


def socket_server():
    '''
    Funcion de servidor 1, esta es la primera que se ejecuta antes de comenzar la batalla
    :return:
    '''
    global nombre_jugador, numero2, nombre_oponente, numero, recibido, poke_team, matriz_tablero, lista_sprites_p1, \
        lista_sprites_p2, data
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    direccion_server = ('192.168.100.5', 8007)
    print("Starting up on port: " + "8007")
    sock.bind(direccion_server)
    sock.listen(1)
    numero = randint(0, 100)
    while True:
        print("Esperando conexion")
        (connection, client_adress) = sock.accept()
        try:
            print("Conection from: ", client_adress)
            while True:
                try:
                    data = pickle.loads(connection.recv(1024))
                except EOFError or ConnectionAbortedError:
                    pass
                print("Recieved:", data)
                if data:
                    if data["tipo_mensaje"] == "inicio_lucha":
                        data["servidor"]["nombre"] = nombre_jugador.get_entrenador()
                        data["servidor"]["numero"] = numero
                        numero2 = data["cliente"]["numero"]
                        nombre_oponente = data["cliente"]["nombre"]
                        encontrar_jugador1(numero, numero2)
                    if data["tipo_mensaje"] == "juego":
                        lista_pokemones_banca(data, poke_team)
                        data["jugador2"]["nombre"] = nombre_jugador.get_entrenador()
                        data["tablero"] = matriz_tablero
                        (lista_sprites_p1, lista_sprites_p2) = set_pokemon_sprites(data["jugador1"]["pokemones_banca"],
                                                                                   data["jugador2"]["pokemones_banca"])
                        print(lista_sprites_p1, lista_sprites_p2)
                    print("Sending data back to client")
                    connection.sendall(pickle.dumps(data, -1))
                    encontrar_jugador1(numero, numero2)
                    print(identificador)
                    recibido = True
                else:
                    print("No more data from:", client_adress)
                    break
        finally:
            connection.close()


def socket_server_2():
    '''
    Esta es la funcion 2 del servidor, esta es la que siempre correra en el fondo, y se encarga de responder adecuadamente a todos loas mensajes del cliente
    :return:
    '''
    global nombre_jugador, numero2, nombre_oponente, numero, recibido, poke_team, matriz_tablero, lista_sprites_p1, \
        lista_sprites_p2, data, tablero_activo, auxiliar, tablero_auxiliar
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    direccion_server = ('192.168.43.239', 8007)
    auxiliar = []
    tablero_auxiliar = [[0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0]]
    print("Starting up on port: " + "8007")
    sock.bind(direccion_server)
    sock.listen(1)
    numero = randint(0, 100)
    while True:
        print("Esperando conexion")
        (connection, client_adress) = sock.accept()
        try:
            print("Conection from: ", client_adress)
            while True:
                try:
                    data = pickle.loads(connection.recv(1024))
                except EOFError or ConnectionAbortedError:
                    pass
                print("Recieved:", data)
                if data:
                    if data["tipo_mensaje"] == "inicio_lucha":
                        data["servidor"]["nombre"] = nombre_jugador.get_entrenador()
                        data["servidor"]["numero"] = numero
                        numero2 = data["cliente"]["numero"]
                        nombre_oponente = data["cliente"]["nombre"]
                        encontrar_jugador1(numero, numero2)
                    if data["tipo_mensaje"] == "juego" and data["jugador2"]["pokemones_banca"] == []:
                        lista_pokemones_banca(data, poke_team)
                        data["jugador2"]["nombre"] = nombre_jugador.get_entrenador()
                        matriz_tablero = data["tablero"]
                        (lista_sprites_p1, lista_sprites_p2) = set_pokemon_sprites(data["jugador1"]["pokemones_banca"],
                                                                                   data["jugador2"]["pokemones_banca"])
                    if data["tipo_mensaje"] == "juego" and data["jugador2"]["pokemones_banca"] != []:
                        data["jugador2"]["pokemones_tablero"] = auxiliar
                        data["tablero"] = tablero_auxiliar
                    print("Sending data back to client")
                    connection.sendall(pickle.dumps(data, -1))
                    recibido = True
                else:
                    print("No more data from:", client_adress)
                    break
        except ConnectionAbortedError:
            connection.close()
            tablero_activo = True


def socket_client(mensaje):
    '''
    Funcion cliente del socket, esta se encarga de enviar los mensajes al socket servidor
    :param mensaje: Parametro que contendra el mensaje a ser enviado por el cliente
    :return:
    '''
    global nombre_jugador, numero, nombre_oponente, numero2, recibido, lista_sprites_p1, lista_sprites_p2, \
        matriz_tablero, tablero_activo, data
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_adress = ("192.168.100.5", 8007)
    print("Conctando a:", server_adress)
    sock.connect(server_adress)
    try:
        print("Sending:", mensaje)
        sock.sendall(pickle.dumps(mensaje, -1))
        amount_recieved = 0
        if recibido:
            amount_total = 4
        else:
            amount_total = 3
        while amount_recieved != amount_total:
            data = pickle.loads(sock.recv(1024))
            amount_recieved = len(data)
            print("Recibido:", data)
            if data["tipo_mensaje"] == "inicio_lucha":
                nombre_oponente = data["servidor"]["nombre"]
                numero2 = data["servidor"]["numero"]
                encontrar_jugador1(numero, numero2)
                print(identificador)
                recibido = True
            if data["tipo_mensaje"] == "juego" and data["jugador1"]["pokemones_tablero"] == []:
                (lista_sprites_p1, lista_sprites_p2) = set_pokemon_sprites(data["jugador1"]["pokemones_banca"],
                                                                           data["jugador2"]["pokemones_banca"])
                print(lista_sprites_p1, lista_sprites_p2)
                tablero_activo = True
            if data["tipo_mensaje"] == "juego":
                matriz_tablero = data["tablero"]
    finally:
        print("Cerrando socket")
        sock.close()


def tablero_batalla_server(lista_sprites_p1, lista_sprites_p2):
    '''
    Esta fucnion corre el tablero de batalla en modo servidor
    :param lista_sprites_p1: Lista que contiene los objetos sprites a dibujar en pantalla del jugador 1
    :param lista_sprites_p2: Lista que contiene los objetos sprites a dibujar en pantalla del jugador 2
    '''
    global matriz_tablero, data, mi_turno, auxiliar, tablero_auxiliar
    thread_music.start()
    pantalla = pygame.display.set_mode((700, 700))
    mi_turno = False
    pygame.display.set_caption("Pokemon Duel")
    lista_p1 = []
    lista_p2 = []
    salir = False
    lista_posiciones = comienza_juego(pantalla, matriz_tablero, lista_sprites_p1, lista_sprites_p2, data, lista_p1,
                                      lista_p2)
    tablero_activo = True
    reloj1 = pygame.time.Clock()
    gris = (184, 184, 184)
    primer_movimiento = True
    pantalla_pelea = False
    lista_sprites = lista_p1
    lista_oponente = lista_p2
    (mouse_x, mouse_y) = pygame.mouse.get_pos()
    poke_team = []
    poke_team_2 = []
    seleccionado = -1
    correcion_seleccionado = 0
    for pokemon in range(len(data["jugador1"]["pokemones_banca"])):
        poke_team.append(nombres_pokemon.index(data["jugador2"]["pokemones_banca"][pokemon]["nombre"]) + 1)
    for pokemon in range(len(data["jugador2"]["pokemones_banca"])):
        poke_team_2.append(nombres_pokemon.index(data["jugador1"]["pokemones_banca"][pokemon]["nombre"]) + 1)
    while salir != True:
        (mouse_x, mouse_y) = pygame.mouse.get_pos()
        if mi_turno:
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for sprite in range(len(lista_sprites)):
                        if lista_sprites[sprite].rect.collidepoint(mouse_x, mouse_y):
                            if identificador == "host":
                                if lista_sprites[sprite].rect.top == 16:
                                    primer_movimiento = True
                            if identificador == "client":
                                if lista_sprites[sprite].rect.top == 600:
                                    primer_movimiento = True
                            seleccionado = sprite
                if seleccionado != -1 and event.type == pygame.MOUSEBUTTONDOWN:
                    if seleccionar_tablero(lista_sprites, seleccionado, matriz_tablero, mouse_x, mouse_y,
                                           lista_posiciones, pantalla, lista_oponente, primer_movimiento,
                                           identificador):
                        if primer_movimiento:
                            x = (mouse_x // 100)
                            y = (mouse_y // 100) - 1
                            indice_1 = poke_team[seleccionado]
                            for i in range(len(data["jugador2"]["pokemones_banca"])):
                                if data["jugador2"]["pokemones_banca"][i]["nombre"] == nombres_pokemon[indice_1 - 1]:
                                    indice_cambiar = i
                                    break
                            cambiar = data["jugador2"]["pokemones_banca"][indice_cambiar]
                            cambiar["posicion"] = (x, y)
                            data["jugador2"]["pokemones_tablero"].append(cambiar)
                            data["jugador2"]["pokemones_banca"].remove(cambiar)
                            data["tablero"] = matriz_tablero
                        else:
                            x = (mouse_x // 100)
                            y = (mouse_y // 100) - 1
                            indice_1 = poke_team[seleccionado]
                            for i in range(len(data["jugador2"]["pokemones_tablero"])):
                                if data["jugador2"]["pokemones_tablero"][i]["nombre"] == nombres_pokemon[indice_1 - 1]:
                                    indice_cambiar = i
                                    break
                            data["jugador2"]["pokemones_tablero"][indice_cambiar]["posicion"] = (x, y)
                            data["tablero"] = matriz_tablero
                            primer_movimiento = False
                        auxiliar = data["jugador2"]["pokemones_tablero"]
                        tablero_auxiliar = data["tablero"]
                        seleccionado = -1
                        mi_turno = False
                        if detectar_pelea(matriz_tablero):
                            pantalla_pelea = True
                            random_1 = randint(0, 100)
                            random_2 = randint(0, 100)
                            pelea_pokemones(pantalla, lista_sprites, lista_oponente, poke_team, poke_team_2,
                                            poke_team[seleccionado + 1], poke_team_2[seleccionado + 1], random_1,
                                            random_2)
                            if random_1 > random_2:
                                matriz_tablero[lista_oponente[seleccionado + 1].rect.top // 100][
                                    lista_oponente[seleccionado + 1].rect.left // 100] = 0
                                lista_oponente[seleccionado + 1].rect.left = 250
                                lista_oponente[seleccionado + 1].rect.top = 16
                            else:
                                matriz_tablero[lista_sprites[seleccionado + 1].rect.top // 100][
                                    lista_sprites[seleccionado + 1].rect.left // 100] = 0
                                lista_sprites[seleccionado + 1].rect.left = 250
                                lista_sprites[seleccionado + 1].rect.top = 600
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for sprite in range(len(lista_sprites)):
                        if lista_sprites[sprite].rect.collidepoint(mouse_x, mouse_y):
                            if identificador == "host":
                                if lista_sprites[sprite].rect.top == 16:
                                    primer_movimiento = True
                            if identificador == "client":
                                if lista_sprites[sprite].rect.top == 600:
                                    primer_movimiento = True
                            seleccionado = sprite
                if event.type == pygame.QUIT:
                    salir = True
            pygame.display.update()
        elif data["jugador1"]["pokemones_tablero"] != []:
            time.sleep(3)
            for pokemon in range(len(data["jugador1"]["pokemones_tablero"])):
                (x, y) = data["jugador1"]["pokemones_tablero"][pokemon]["posicion"]
                matriz_tablero[y][x] = 1
                (x, y) = (50 + (x * 100), 100 + (y * 100))
                numero_sprite = poke_team.index(
                    nombres_pokemon.index(data["jugador1"]["pokemones_tablero"][pokemon]["nombre"]) + 1)
                lista_posiciones = dibujar_tablero(pantalla, matriz_tablero, gris)
                dibujar_pokemon(pantalla, lista_oponente, (x - 35, y - 35), numero_sprite)
                (lista_oponente[numero_sprite].rect.left, lista_oponente[numero_sprite].rect.top) = (x - 35, y - 35)
                for poke in lista_sprites:
                    dibujar_pokemon(pantalla, lista_sprites, (x, y), -1)
                mi_turno = True
            pygame.display.update()


def tablero_batalla_cliente(lista_sprites_p1, lista_sprites_p2):
    '''
    Muestra y hace funcionar el tablero de batalla en modo cliente
    :param lista_sprites_p1: Lista que contiene los objetos sprites a dibujar en pantalla del jugador 1
    :param lista_sprites_p2: Lista que contiene los objetos sprites a dibujar en pantalla del jugador 2
    :return:
    '''
    global matriz_tablero, data, mi_turno
    thread_music.start()
    pantalla = pygame.display.set_mode((700, 700))
    pygame.display.set_caption("Pokemon Duel")
    lista_p1 = []
    lista_p2 = []
    salir = False
    lista_posiciones = comienza_juego(pantalla, matriz_tablero, lista_sprites_p1, lista_sprites_p2, data, lista_p1,
                                      lista_p2)
    tablero_activo = True
    reloj1 = pygame.time.Clock()
    gris = (184, 184, 184)
    primer_movimiento = True
    pantalla_pelea = False
    lista_sprites = lista_p2
    lista_oponente = lista_p1
    (mouse_x, mouse_y) = pygame.mouse.get_pos()
    poke_team = []
    poke_team_2 = []
    seleccionado = -1
    correcion_seleccionado = 0
    for pokemon in range(len(data["jugador1"]["pokemones_banca"])):
        poke_team.append(nombres_pokemon.index(data["jugador1"]["pokemones_banca"][pokemon]["nombre"]) + 1)
    for pokemon in range(len(data["jugador2"]["pokemones_banca"])):
        poke_team_2.append(nombres_pokemon.index(data["jugador1"]["pokemones_banca"][pokemon]["nombre"]) + 1)
    while salir != True:
        if mi_turno:
            (mouse_x, mouse_y) = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for sprite in range(len(lista_sprites)):
                        if lista_sprites[sprite].rect.collidepoint(mouse_x, mouse_y):
                            if identificador == "host":
                                if lista_sprites[sprite].rect.top == 16:
                                    primer_movimiento = True
                            if identificador == "client":
                                if lista_sprites[sprite].rect.top == 600:
                                    primer_movimiento = True
                            seleccionado = sprite
                if seleccionado != -1 and event.type == pygame.MOUSEBUTTONDOWN:
                    if seleccionar_tablero(lista_sprites, seleccionado, matriz_tablero, mouse_x, mouse_y, lista_posiciones,
                                           pantalla, lista_oponente, primer_movimiento, identificador):
                        if primer_movimiento:
                            x = (mouse_x // 100)
                            y = (mouse_y // 100) - 1
                            indice_1 = poke_team[seleccionado]
                            for i in range(len(data["jugador1"]["pokemones_banca"])):
                                if data["jugador1"]["pokemones_banca"][i]["nombre"] == nombres_pokemon[indice_1 - 1]:
                                    indice_cambiar = i
                                    break
                            cambiar = data["jugador1"]["pokemones_banca"][indice_cambiar]
                            cambiar["posicion"] = (x, y)
                            data["jugador1"]["pokemones_tablero"].append(cambiar)
                            data["jugador1"]["pokemones_banca"].remove(cambiar)
                            data["tablero"] = matriz_tablero
                            socket_client(data)

                        else:
                            x = (mouse_x // 100)
                            y = (mouse_y // 100) - 1
                            indice_1 = poke_team[seleccionado]
                            for i in range(len(data["jugador1"]["pokemones_tablero"])):
                                if data["jugador1"]["pokemones_tablero"][i]["nombre"] == nombres_pokemon[indice_1 - 1]:
                                    indice_cambiar = i
                                    break
                            data["jugador1"]["pokemones_tablero"][indice_cambiar]["posicion"] = (x, y)
                            data["tablero"] = matriz_tablero
                            socket_client(data)
                        seleccionado = -1
                        primer_movimiento = False
                        if data["jugador2"]["pokemones_tablero"] != []:
                            for pokemon in range(len(data["jugador2"]["pokemones_tablero"])):
                                (x, y) = data["jugador2"]["pokemones_tablero"][pokemon]["posicion"]
                                matriz_tablero[y][x] = 2
                                (x, y) = (50 + (x * 100), 100 + (y * 100))
                                numero_sprite = poke_team_2.index(
                                    nombres_pokemon.index(data["jugador2"]["pokemones_tablero"][pokemon]["nombre"]) + 1)
                                lista_posiciones = dibujar_tablero(pantalla, matriz_tablero, gris)
                                dibujar_pokemon(pantalla, lista_oponente, (x - 35, y - 35), numero_sprite)
                                (lista_oponente[numero_sprite].rect.left, lista_oponente[numero_sprite].rect.top) = (
                                x - 35, y - 35)
                                for poke in lista_sprites:
                                    dibujar_pokemon(pantalla, lista_sprites, (x, y), -1)
                        if detectar_pelea(matriz_tablero):
                            pantalla_pelea = True
                            random_1 = randint(0, 100)
                            random_2 = randint(0, 100)
                            pelea_pokemones(pantalla, lista_sprites, lista_oponente, poke_team, poke_team_2,
                                            poke_team[seleccionado + 1], poke_team_2[seleccionado + 1], random_1, random_2)
                            if random_1 > random_2:
                                matriz_tablero[lista_oponente[seleccionado + 1].rect.top // 100][
                                    lista_oponente[seleccionado + 1].rect.left // 100] = 0
                                lista_oponente[seleccionado + 1].rect.left = 250
                                lista_oponente[seleccionado + 1].rect.top = 16
                            else:
                                matriz_tablero[lista_sprites[seleccionado + 1].rect.top // 100][
                                    lista_sprites[seleccionado + 1].rect.left // 100] = 0
                                lista_sprites[seleccionado + 1].rect.left = 250
                                lista_sprites[seleccionado + 1].rect.top = 600
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for sprite in range(len(lista_sprites)):
                        if lista_sprites[sprite].rect.collidepoint(mouse_x, mouse_y):
                            if identificador == "host":
                                if lista_sprites[sprite].rect.top == 16:
                                    primer_movimiento = True
                            if identificador == "client":
                                if lista_sprites[sprite].rect.top == 600:
                                    primer_movimiento = True
                            seleccionado = sprite
                if event.type == pygame.QUIT:
                    salir = True
            pygame.display.update()

def encontrar_jugador1(numero1, numero2):
    global identificador
    if numero1 > numero2:
        identificador = "host"
    else:
        identificador = "client"


# Esta funcion se encarga de calcular si se puede capturar o no al pokemon salvage solo si este posee menos del  40% de su
# vida inicial
# Entradas: vida_wild_poke sera la vida actual del pokemon en batalla, vida_fija sera un numero fijo de vida maximo para
# el pokemon salvage
def capturar_pokemon(vida_wild_poke):
    global poke_number, ventana_principal, item_menu, capturado_bool, pokemon_en_batalla, poke_team, vida_wild, \
        nombre_poke_wild, nombre_poke_red, poke_box, message_box_one, mensaje, vida_enemigo, vida_wild_fija
    ventana_principal.delete("wild_poke")
    numero_pokemones = contar_pokemon_en_lista(poke_team, 0)
    if vida_wild_poke < (vida_wild_fija * 0.4):
        if numero_pokemones >= 6:
            poke_box.append([poke_number, vida_wild_fija, vida_wild_fija])
        else:
            poke_team.append([poke_number, vida_wild_fija, vida_wild_fija])
        item_menu = False
        capturado_bool = True
        reiniciar_vida(poke_team)
        vida_wild = 200
        message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
        mensaje = Label(ventana_principal, text=nombres_pokemon[poke_number - 1] + " was Caught"
                        , font=("Pokemon R/B/Y", 15))
        mensaje.place(x=100, y=450)
    else:
        wild_pokemon_1 = ventana_principal.create_image((450, 150), image=wild_pokemon_battle_1,
                                                        tag="wild_poke_1")
        ventana_principal.delete("capture_ball")
        nombre_poke_wild.destroy()
        nombre_poke_red.destroy()
        vida_enemigo.destroy()
        pelea_2(pokemon_en_batalla)
        item_menu = False


# Funcion recursiva para reiniciar al 100% las vidas de todos los pokemones en el equipo
# Entrada: poke_team sera la lista de los pokemones en el equipo
def reiniciar_vida(poke_team):
    if len(poke_team) == 0:
        return True
    else:
        poke_team[0][1] = poke_team[0][2]
        return reiniciar_vida(poke_team[1:])


# Funcion para regresar al bosque luego de finalizada un batalla
# bosque_ident sirve para identificar que parte del bosque desea cambiar, capturado valor numerico que representa cual
# pokemon salvage fue capturado, coordenadas_red son las coordenadas del personaje antes de entrar en batalla,
# coordenadas pikachu son las coordenadas de pikachu antes de entrar en batalla, huir valor booleano que dice si se
# esta regresando al bosque con la opcion de huir
def volver_bosque(bosque_ident, capturado, coordenadas_red, coordenadas_pickachu, huir):
    global poke_list, ventana_principal, nombre_poke_wild, vida_enemigo, Red, pikachu, mensaje, capturado_bool, music \
        , back_ident
    if bosque_ident == "ver_for_1":
        back_ident = cambiar_background(bosque_ident, ventana_principal, background_sprite, background)
    elif bosque_ident == "ver_for_2":
        back_ident = cambiar_background(bosque_ident, ventana_principal, background_sprite, background)
    elif bosque_ident == "ver_for_3":
        back_ident = cambiar_background(bosque_ident, ventana_principal, background_sprite, background)
    elif bosque_ident == "ver_for_4":
        back_ident = cambiar_background(bosque_ident, ventana_principal, background_sprite, background)
    elif bosque_ident == "ver_for_5":
        back_ident = cambiar_background(bosque_ident, ventana_principal, background_sprite, background)
    draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
    ventana_principal.delete("capture_ball")
    ventana_principal.delete("pokemon_seleccionado")
    if huir:
        delete_captured_poke(poke_list, capturado, ventana_principal)
    else:
        pass
    nombre_poke_wild.destroy()
    vida_enemigo.destroy()
    Red = ventana_principal.create_image((coordenadas_red[0], coordenadas_red[1]), image=Red_sprite, tag="Red")
    pikachu = ventana_principal.create_image((coordenadas_pickachu[0], coordenadas_pickachu[1]), image=pikachu_sprite,
                                             tag="pikachu")
    music = "music/viridian_forest.mp3"
    pygame.mixer.music.stop()
    pygame.mixer.music.load(music)
    pygame.mixer.music.play(-1)
    if capturado_bool:
        mensaje.destroy()
    else:
        pass
    ventana_principal.delete("message_box")


# Funcion recurvisa que cuenta la cantidad de pokemones en el equipo
# Entraadas: poke_team es la lista con los pokemones en el equipo, contador sera el valor de cola a retornar
def contar_pokemon_en_lista(poke_team, contador):
    if len(poke_team) == 0:
        return contador
    else:
        return contar_pokemon_en_lista(poke_team[1:], contador + 1)


# Funcion recursiva que muestra los label de los nombres de los pokemones en el equipo
# Entradas: poke_team lista que contiene los pokemones en el equipo y label_lista lista donde se almacenan los label
# de los nombres
def mostrar_pokemon(poke_team, name_label_list):
    global poke_list_team, ventana_principal, menu_activo
    menu_activo = False
    poke_list_team = True
    item_box_show = ventana_principal.create_image((300, 300), image=item_box, tag="itembox")
    imprimir_nombres_pokemon(poke_team, name_label_list, 170, 240, 10, 20)


# Funcion recursiva que imprimira todos los label de los nombres de los pokemones en el equipo
# Entradas: poke_team lista de los pokemones en el equipo, name_labe_lista donde se almacenaran los label de cada
# nombre de un pokemone en el equipo, x pocision x donde dibujar en la pantalla, y posicion y donde comenzar a dibujar
# en la pantalla, size tamano de la fuente a utilizar, space es el espacio de separacion entre los nombres
def imprimir_nombres_pokemon(poke_team, name_label_list, x, y, size, space):
    if len(poke_team) == 0:
        return name_label_list
    else:
        nombre = Label(ventana_principal, text=nombres_pokemon[int(poke_team[0][0]) - 1], font=("Pokemon R/B/Y", size),
                       bg="white")
        nombre.place(x=x, y=y)
        name_label_list.append(nombre)
        return imprimir_nombres_pokemon(poke_team[1:], name_label_list, x, y + space, size, space)


# Funcion para realizar el cambio de pokemon en la interfaz de batalla
# Entrada poke_team lista de pokemones disponibles en el equipo
def cambiar_pokemon(poke_team):
    global selected_pokemon, pokemon_en_batalla, mensaje_fainted, mensaje
    coordenadas_flecha = ventana_principal.coords("flecha")
    if coordenadas_flecha[1] == 100:
        if poke_team[0][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 0
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[0][0]) + ".png")
    elif coordenadas_flecha[1] == 140:
        if poke_team[1][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 1
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[1][0]) + ".png")
    elif coordenadas_flecha[1] == 180:
        if poke_team[2][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 2
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[2][0]) + ".png")
    elif coordenadas_flecha[1] == 220:
        if poke_team[3][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 3
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[3][0]) + ".png")
    elif coordenadas_flecha[1] == 260:
        if poke_team[4][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 4
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[4][0]) + ".png")
    elif coordenadas_flecha[1] == 300:
        if poke_team[5][1] <= 0:
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
            mensaje = Label(ventana_principal, text="Pokemon has no energy", font=("Pokemon R/B/Y", 15))
            mensaje.place(x=100, y=450)
            mensaje_fainted = True
        else:
            pokemon_en_batalla = 5
            selected_pokemon = PhotoImage(file="sprites/pokemon/back/" + str(poke_team[5][0]) + ".png")


# Funcion para eliminar los label con los nombres de los pokemon que se encuentra en name_label_list
# Entradas: name_labe_list lista con todos los labels de los nombres
def borrar_label_pokemon(name_label_list):
    if len(name_label_list) == 0:
        return True
    else:
        name_label_list[0].destroy()
        return borrar_label_pokemon(name_label_list[1:])


# Funcion que dibuja el menu en la pantalla y cambia el valor booleano de la variable indicadora de activacion del menu
def menu():
    global ventana_principal, menu_show, menu_sprite, menu_activo
    menu_show = ventana_principal.create_image((500, 150), image=menu_sprite, tag="menu")
    fleha_menu = ventana_principal.create_image((435, 85), image=flecha_seleccion, tag="flecha_3")
    menu_activo = True


# Funcion que limita las partes del juego en el cual se puede abrir el menu
def abrir_menu():
    global back_ident
    if back_ident == "prof_intro_1" or back_ident == "prof_intro_2" or back_ident == "prof_intro_3" or \
                    back_ident == "prof_intro_4" or back_ident == "battle_1" or back_ident == "battle_2" or \
                    back_ident == "choose":
        return False
    else:
        return True


# Funcion para borrar el menu de la pantalla
def borrar_menu():
    global menu_activo
    ventana_principal.delete("menu")
    ventana_principal.delete("flecha_3")
    menu_activo = False


# Funcion realizada con el fin de cambiar la pocision de las flechas de indicacion
# Entradas: x indica la cantidad a desplazarse en x, y indica la cantidad a desplazarce en y
def mover_flecha(x, y):
    global ventana_principal, menu_activo
    coordenadas_flecha = ventana_principal.coords("flecha")
    if back_ident == "battle_2":
        if y < 0 and coordenadas_flecha[1] == 485.0:
            return False
        elif y > 0 and coordenadas_flecha[1] == 545.0:
            return False
        elif x < 0 and coordenadas_flecha[0] == 280.0:
            return False
        elif x > 0 and coordenadas_flecha[0] == 460.0:
            return False
        else:
            new_coordinates = [coordenadas_flecha[0] + x, coordenadas_flecha[1] + y]
            ventana_principal.coords("flecha", new_coordinates[0], new_coordinates[1])
    elif back_ident == "choose":
        if y < 0 and coordenadas_flecha[1] == 100:
            return False
        elif y > 0 and coordenadas_flecha[1] == 300:
            return False
        else:
            new_coordinates = [coordenadas_flecha[0] + x, coordenadas_flecha[1] + y]
            ventana_principal.coords("flecha", new_coordinates[0], new_coordinates[1])
    elif menu_activo:
        coordenadas_flecha_tres = ventana_principal.coords("flecha_3")
        if y < 0 and coordenadas_flecha_tres[1] == 85:
            return False
        elif y > 0 and coordenadas_flecha_tres[1] == 245:
            return False
        else:
            new_coordinates = [coordenadas_flecha_tres[0] + x, coordenadas_flecha_tres[1] + y]
            ventana_principal.coords("flecha_3", new_coordinates[0], new_coordinates[1])
            print(ventana_principal.coords("flecha_3"))
    elif back_ident == "gym":
        coordenadas_flecha_gym = ventana_principal.coords("flecha_gym")
        if y < 0 and coordenadas_flecha_gym[1] == 290:
            return False
        elif y > 0 and coordenadas_flecha_gym[1] == 370:
            return False
        else:
            new_coordinates = [coordenadas_flecha_gym[0] + x, coordenadas_flecha_gym[1] + y]
            ventana_principal.coords("flecha_gym", new_coordinates[0], new_coordinates[1])
            print(new_coordinates)


def obtener_ip_port():
    '''
    Obtiene la direccion ip en la que se monta el servidor y la muestra en pantalla
    '''
    global lblip, servidor_estado, ventana_principal, lblport, port, lblesperando
    if servidor_estado:
        port = 8007
        lblip = Label(ventana_principal, text="IP: " + str(socket.gethostbyname(socket.gethostname())),
                      font=("Pokemon R/B/Y", 12))
        lblip.place(x=135.0, y=300)
        lblport = Label(ventana_principal, text="Puerto: " + str(port), font=("Pokemon R/B/Y", 12))
        lblport.place(x=135, y=320)


def inicio_batalla():
    '''
    Funcion para cambiar el fondo al inicio de la batalla
    :return:
    '''
    global back_ident, background_sprite, ventana_principal, background, estado_batalla
    if estado_batalla:
        back_ident = cambiar_background("inicio_batalla", ventana_principal, background_sprite, background)


def nombre_numero():
    '''
    Funcion que se encarga de mostrar los numeros y los nombres de los jugadores conectados
    :return:
    '''
    global lblnombres, lblnumero, nombre, nombre_oponente, nombre_jugador, numero, data, ventana_principal, lblip, lblport, numero2, servidor_estado, identificador
    print(servidor_estado)
    ventana_principal.delete("flecha_gym")
    lblnombres = Label(text=nombre_jugador.get_entrenador(), font=("Pokemon R/B/Y", 12))
    lblnombres.place(x=150, y=550)
    lblnumero = Label(text=str(numero), font=("Pokemon R/B/Y", 14))
    lblnumero.place(x=40, y=420)
    lblnombre_oponente = Label(text=nombre_oponente, font=("Pokemon R/B/Y", 12))
    lblnombre_oponente.place(x=470, y=150)
    if servidor_estado:
        lblip.destroy()
        lblport.destroy()
    if identificador == "host":
        lblnumero_oponente = Label(text=str(numero2), font=("Pokemon R/B/Y", 14))
        ventana_principal.delete("pickachu")
        lblnumero_oponente.place(x=420, y=50)
    if identificador == "client":
        lblnumero_oponente = Label(text=str(numero2), font=("Pokemon R/B/Y", 14))
        lblnumero_oponente.place(x=420, y=50)
    ventana_principal.delete("pickachu")


def ganador():
    global ventana_principal, lblfinal, nombre_jugador, nombre_oponente, ganador_cliente, ganador_host
    if ganador_cliente:
        lblfinal = Label(text=nombre_jugador.get_entrenador(), font=("Pokemon R/B/Y", 16))
        lblfinal.place(x=260, y=550)
    elif ganador_host:
        lblfinal = Label(text=nombre_oponente, font=("Pokemon R/B/Y", 16))
        lblfinal.place(x=260, y=550)


def en_batalla():
    '''
    Funcion que se encarga de definir si se encuentra batallando o no
    :return:
    '''
    global estado_batalla
    estado_batalla = True
    inicio_batalla()


# obtener nombre
# Entrada:sin argumentos
def obtener_nombre():
    global nombre_texto, lblnombre, nombre, nombre_2
    # si estamos en el intro 3 hace un texto para escribir el nombre y lo guarda en una variable global
    # y cuando se preciona el enter llama a la funcion key_enter
    if back_ident == "prof_intro_3":
        nombre = StringVar()
        lblnombre = Label(text="nombre:", font=("Pokemon R/B/Y", 12))
        lblnombre.place(x=30, y=450)
        nombre_texto = Entry(ventana_principal, font=("Pokemon R/B/Y", 12), textvariable=nombre)
        nombre_texto.place(x=125, y=450)
        nombre_texto.focus_set()
        nombre_texto.bind("<Return>", key_enter)
        update_nombre()


def update_nombre():
    '''
    Funcion para actualizar el valor del nombre del oponente
    :return:
    '''
    global nombre, nombre_2
    nombre_2 = nombre_texto.get()


def lista_pokemones_banca(mensaje, poketeam):
    '''
    Genera los pokemones de la banca dependiendo de los pokemones que se tenga en el equipo
    :param mensaje: mensaje a enviar para agregarle los pokemones de la banca al mensaje
    :param poketeam: lista que contiene los numeros de los pokemones en el equipo
    :return:
    '''
    global identificador, nombres_pokemon
    if identificador == "host":
        x_inicial = 100
        for pokemon in poketeam:
            mensaje["jugador2"]["pokemones_banca"].append(
                {"nombre": nombres_pokemon[int(pokemon[0]) - 1], "posicion": (x_inicial, 600)})
            x_inicial += 100
    if identificador == "client":
        x_inicial = 100
        for pokemon in poketeam:
            mensaje["jugador1"]["pokemones_banca"].append(
                {"nombre": nombres_pokemon[int(pokemon[0]) - 1], "posicion": (x_inicial, 16)})
            x_inicial += 100


def set_pokemon_sprites(pokemones_jugador_1, pokemones_jugador_2):
    '''
    funcion que se encarga de cargar sprites dependiendo de los pokmones en el equipo
    :param pokemones_jugador_1: Lista de pokemones del jugador 1
    :param pokemones_jugador_2: Lista de pokemones del jugador 2
    :return: dos listas, cada una con los sprites de los pokemones de cada jugador
    '''
    global nombres_pokemon
    sprites_p1 = []
    sprites_p2 = []
    pokemones_1 = []
    pokemones_2 = []
    for i in pokemones_jugador_1:
        pokemones_1.append(nombres_pokemon.index(i["nombre"]) + 1)

    for i in pokemones_jugador_2:
        pokemones_2.append(nombres_pokemon.index(i["nombre"]) + 1)
    (sprites_p1, sprites_p2) = crear_sprites_pokemon(pokemones_1, pokemones_2)
    return (sprites_p1, sprites_p2)


# Funcion recursiva que realiza la animacion de captrua del pokemon salvage
# Entrada: contador que limita la cantidad de veces que se realiza la recursividad
def animacion_capture(cont):
    global ventana_principal, main
    if cont == 11:
        return False
    else:
        coordenadas = ventana_principal.coords("capture_ball")
        ventana_principal.coords("capture_ball", coordenadas[0] + 25, coordenadas[1] - 17)
        main.update()
        time.sleep(0.025)
        return animacion_capture(cont + 1)


# Funcion que realiza la animacion de ataque del pokemon del jugador
# Entrada: contador que limita la cantidad de veces que se realiza la recursividad
def animacion_pelea(cont):
    global ventana_principal, main
    if cont == 11:
        return False
    else:
        coordenadas = ventana_principal.coords("attack")
        ventana_principal.coords("attack", coordenadas[0] + 25, coordenadas[1] - 17)
        main.update()
        time.sleep(0.025)
        return animacion_pelea(cont + 1)


# Funcion que realiza la animacion de ataque del pokemon salvage
# Entrada: contador que limita la cantidad de veces que se realiza la recursividad
def animacion_pelea_2(cont):
    global ventana_principal, main, atacando
    if cont == 11:
        atacando = False
        return False
    else:
        coordenadas = ventana_principal.coords("attack_2")
        ventana_principal.coords("attack_2", coordenadas[0] - 25, coordenadas[1] + 17)
        main.update()
        time.sleep(0.025)
        return animacion_pelea_2(cont + 1)


def choca_oak(new_coordinates):
    coordenadas_oak = ventana_principal.coords("oak")
    if new_coordinates == coordenadas_oak:
        return True
    else:
        return False


def gym_menu():
    '''
    Funcion que muestra el menu del gimnasio
    :return:
    '''
    global menu_gym
    flecha_gym = ventana_principal.create_image((135, 290), image=flecha_seleccion, tag="flecha_gym")
    menu_gym = True


# Funcion que incluye sub funciones de que realizar si se presiona la tecla hacia arriba
def keyup(event):
    global recibido
    if back_ident == "battle_2":
        mover_flecha(0, -60)
    elif back_ident == "choose":
        mover_flecha(0, -40)
    elif menu_activo:
        mover_flecha(0, -80)
    elif menu_gym:
        mover_flecha(0, -80)
    elif recibido:
        pass
    else:
        red_camina_arriba(contador_arriba)


# Funcion que incluye sub funciones de que realizar si se presiona la tecla hacia abajo
def keydown(event):
    global recibido
    coordenadas_flecha = ventana_principal.coords("flecha")
    if back_ident == "battle_2":
        mover_flecha(0, 60)
    elif back_ident == "choose":
        mover_flecha(0, 40)
    elif menu_activo:
        mover_flecha(0, 80)
    elif menu_gym:
        mover_flecha(0, 80)
    elif recibido:
        pass
    else:
        red_camina_abajo(contador_abajo)


# Funcion que incluye sub funciones de que realizar si se presiona la tecla hacia la izquierda
def keyleft(event):
    global recibido
    if back_ident == "battle_2":
        mover_flecha(-180, 0)
    elif back_ident == "choose":
        pass
    elif menu_activo:
        pass
    elif recibido:
        pass
    else:
        red_camina_izquierda(contador_izquierda)


# Funcion que incluye sub funciones de que realizar si se presiona la tecla hacia la derecha
def keyright(event):
    global recibido
    if back_ident == "battle_2":
        mover_flecha(180, 0)
    elif back_ident == "choose":
        pass
    elif menu_activo:
        pass
    elif recibido:
        pass
    else:
        red_camina_derecha(contador_derecha)


# Funcion que incluye sub funciones de que realizar si se presiona la tecla de seleccion a
def keya(event):
    global ventana_principal, item_menu, capturado_bool, capturado, bosque_proviene, coordenadas_red \
        , coordenadas_pikachu, vida_enemigo, vida_pokemon, poke_team, vida_wild, nombre_poke_red, pokemon_en_batalla, \
        menu_activo, perdio_batalla, poke_number, message_box_one, mensaje, saved, mensaje_fainted, gano_batalla, \
        sin_pokemon, contador_ataques, mensaje_oaak, atacando, back_ident, servidor_estado, thread_socket_servidor, \
        thread_socket_cliente, recibido, identificador, tablero, thread_server_2, nombre_jugador, numero, matriz_tablero \
        , lista_sprites_p1, lista_sprites_p2, data, tablero_activo
    if gano_batalla:
        mensaje.destroy()
        ventana_principal.delete("wild_poke_1")
        gano_batalla = False
        volver_bosque(bosque_proviene, capturado, coordenadas_red, coordenadas_pikachu, True)
    elif recibido:
        cambiar_background("inicio_batalla", ventana_principal, background_sprite, background)
        nombre_numero()
        if identificador == "host":
            time.sleep(5)
            thread_server_2.start()
            recibido = False
            pygame.mixer.music.stop()
            tablero_batalla_server(lista_sprites_p1, lista_sprites_p2)
        if identificador == "client":
            mensaje = {"tipo_mensaje": "juego", "tablero": matriz_tablero,
                       "jugador1": {"nombre": nombre_jugador.get_entrenador(), "pokemones_tablero": [], "pokemones_banca": []},
                       "jugador2": {"nombre": '', "pokemones_tablero": [], "pokemones_banca": []}}
            lista_pokemones_banca(mensaje, poke_team)
            socket_client(mensaje)
            pygame.mixer.music.stop()
            tablero_batalla_cliente(lista_sprites_p1, lista_sprites_p2)
            recibido = False
    elif atacando:
        pass
    elif sin_pokemon:
        sin_pokemon = False
        ventana_principal.delete("flecha")
        mensaje.destroy()
        ventana_principal.delete("wild_poke_1")
        gano_batalla = False
        volver_bosque(bosque_proviene, capturado, coordenadas_red, coordenadas_pikachu, False)
        reiniciar_vida(poke_team)
        vida_wild = 200
    elif perdio_batalla:
        mensaje.destroy()
        ventana_principal.delete("message_box")
        perdio_batalla = False
        nombre_poke_wild.destroy()
        nombre_poke_red.destroy()
        back_ident = cambiar_background("choose", ventana_principal, background_sprite, background)
        flecha_seleccion_1 = ventana_principal.create_image((280, 485), image=flecha_seleccion, tag="flecha")
        ventana_principal.coords("flecha", 85, 100)
        ventana_principal.delete("wild_poke_1")
        ventana_principal.delete("pokemon_seleccionado")
        imprimir_nombres_pokemon(poke_team, name_label_list, 100, 80, 20, 40)
        vida_enemigo.destroy()
        vida_pokemon.destroy()
    elif mensaje_fainted:
        mensaje.destroy()
        ventana_principal.delete("message_box")
        mensaje_fainted = False
        nombre_poke_wild.destroy()
        nombre_poke_red.destroy()
        back_ident = cambiar_background("choose", ventana_principal, background_sprite, background)
        ventana_principal.coords("flecha", 85, 100)
        ventana_principal.delete("wild_poke_1")
        ventana_principal.delete("pokemon_seleccionado")
        imprimir_nombres_pokemon(poke_team, name_label_list, 100, 80, 20, 40)
        vida_enemigo.destroy()
        vida_pokemon.destroy()
    elif saved:
        mensaje.destroy()
        ventana_principal.delete("message_box")
        saved = False
    elif mensaje_fainted:
        mensaje.destroy()
        ventana_principal.delete("message_box")
        mensaje_fainted = False
    elif mensaje_oaak:
        mensaje_oaak = False
        mensaje.destroy()
        ventana_principal.delete("message_oak")
    elif back_ident == "battle_2":
        coordenadas_flecha = ventana_principal.coords("flecha")
        if coordenadas_flecha == [460.0, 485.0]:
            nombre_poke_wild.destroy()
            nombre_poke_red.destroy()
            back_ident = cambiar_background("choose", ventana_principal, background_sprite, background)
            ventana_principal.coords("flecha", 85, 100)
            ventana_principal.delete("wild_poke")
            ventana_principal.delete("pokemon_seleccionado")
            imprimir_nombres_pokemon(poke_team, name_label_list, 100, 80, 20, 40)
            vida_enemigo.destroy()
            vida_pokemon.destroy()
        elif coordenadas_flecha == [280.0, 545.0]:
            items()
        elif item_menu:
            ventana_principal.delete("flecha_2")
            pokeball_2_image = ventana_principal.create_image((175, 375), image=pokeball_2, tag="capture_ball")
            coorde = ventana_principal.coords("capture_ball")
            ventana_principal.delete("itembox")
            pokebola.destroy()
            cantidad_pokebola.destroy()
            animacion_capture(0)
            capturar_pokemon(vida_wild)
        elif capturado_bool:
            volver_bosque(bosque_proviene, capturado, coordenadas_red, coordenadas_pikachu, True)
            pokemon_en_batalla = 0
            capturado_bool = False
        elif coordenadas_flecha == [280.0, 485.0]:
            atacando = True
            contador_ataques = +1
            attack_image = ventana_principal.create_image((175, 375), image=attack, tag="attack")
            animacion_pelea(0)
            if contador_ataques == 4:
                poke_team[pokemon_en_batalla][1] = poke_team[pokemon_en_batalla][1] - 50
            else:
                poke_team[pokemon_en_batalla][1] = poke_team[pokemon_en_batalla][1] - 25
            ventana_principal.delete("attack")
            attack_image_2 = ventana_principal.create_image((450, 150), image=attack_2, tag="attack_2")
            animacion_pelea_2(0)
            vida_wild = vida_wild - 45
            ventana_principal.delete("attack_2")
            vida_pokemon.destroy()
            vida_enemigo.destroy()
            nombre_poke_red.destroy()
            ventana_principal.delete("flecha")
            if poke_team[pokemon_en_batalla][1] <= 0:
                ventana_principal.delete("pokemon_seleccionado")
                capturado_bool = False
                if contar_pokemon_en_lista(poke_team, 0) != 1:
                    perdio_batalla = True
                else:
                    sin_pokemon = True
                message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
                mensaje = Label(ventana_principal, text=nombres_pokemon[int(poke_team[pokemon_en_batalla][0]) - 1]
                                                        + " fainted", font=("Pokemon R/B/Y", 15))
                ventana_principal.delete("wild_poke")
                mensaje.place(x=100, y=450)
            elif vida_wild <= 0:
                pokemon_en_batalla = 0
                ventana_principal.delete("wild_poke_1")
                vida_wild = 200
                reiniciar_vida(poke_team)
                capturado_bool = False
                gano_batalla = True
                message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
                mensaje = Label(ventana_principal, text=nombres_pokemon[poke_number - 1] + " fainted",
                                font=("Pokemon R/B/Y", 15))
                mensaje.place(x=100, y=450)
                ventana_principal.delete("flecha")
                ventana_principal.delete("wild_poke")
            else:
                pelea_2(pokemon_en_batalla)
        elif coordenadas_flecha == [460.0, 545.0]:
            reiniciar_vida(poke_team)
            pokemon_en_batalla = 0
            volver_bosque(bosque_proviene, capturado, coordenadas_red, coordenadas_pikachu, False)
            ventana_principal.delete("wild_poke")
            nombre_poke_red.destroy()
            vida_pokemon.destroy()
            ventana_principal.delete("flecha")
    elif menu_activo:
        coordenadas_flecha_tres = ventana_principal.coords("flecha_3")
        if coordenadas_flecha_tres[1] == 85.0:
            mostrar_pokemon(poke_team, name_label_list)
        elif coordenadas_flecha_tres[1] == 165.0:
            save()
            if saved:
                essage_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_box")
                mensaje = Label(ventana_principal, text="Save Succesfull", font=("Pokemon R/B/Y", 15))
                mensaje.place(x=100, y=450)
    elif back_ident == "battle_1":
        pelea_2(pokemon_en_batalla)
    elif back_ident == "choose":
        cambiar_pokemon(poke_team)
        ventana_principal.delete("flecha")
        borrar_label_pokemon(name_label_list)
        pelea_2(pokemon_en_batalla)
    elif entrar_pelea():
        bosque_proviene = back_ident
        coordenadas_red = ventana_principal.coords("Red")
        coordenadas_pikachu = ventana_principal.coords("pikachu")
        pelea_1()
    elif back_ident == "prof_intro_1":
        back_ident = cambiar_background("prof_intro_2", ventana_principal, background_sprite, background)
    elif back_ident == "prof_intro_2":
        back_ident = cambiar_background("prof_intro_3", ventana_principal, background_sprite, background)
    elif back_ident == "prof_intro_3":
        obtener_nombre()
        back_ident = cambiar_background("prof_intro_4", ventana_principal, background_sprite, background)
    elif back_ident == "prof_lab":
        coordenadas_red = ventana_principal.coords("Red")
        coordenadas_oak = ventana_principal.coords("oak")
        if coordenadas_red[0] + 25 == coordenadas_oak[0] and coordenadas_red[1] == coordenadas_oak[1]:
            mensaje_oaak = True
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
            mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                    + "llevarlo contigo siempre de ahora en adelante"
                            , font=("Pokemon R/B/Y", 8))
            mensaje.place(x=20, y=480)

        elif coordenadas_red[0] - 25 == coordenadas_oak[0] and coordenadas_red[1] == coordenadas_oak[1]:
            mensaje_oaak = True
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
            mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                    + "llevarlo contigo siempre de ahora en adelante"
                            , font=("Pokemon R/B/Y", 8))
            mensaje.place(x=20, y=480)
        elif coordenadas_red[0] == coordenadas_oak[0] and coordenadas_red[1] + 25 == coordenadas_oak[1]:
            mensaje_oaak = True
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
            mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                    + "llevarlo contigo siempre de ahora en adelante"
                            , font=("Pokemon R/B/Y", 8))
            mensaje.place(x=20, y=480)
        elif coordenadas_red[0] == coordenadas_oak[0] and coordenadas_red[1] - 25 == coordenadas_oak[1]:
            mensaje_oaak = True
            message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
            mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                    + "llevarlo contigo siempre de ahora en adelante"
                            , font=("Pokemon R/B/Y", 8))
            mensaje.place(x=20, y=480)
    elif back_ident == "gym":
        coordenadas_flecha_gym = ventana_principal.coords("flecha_gym")
        if coordenadas_flecha_gym == [135.0, 290.0] and recibido == False:
            identificador = "host"
            thread_socket_servidor.start()
        elif coordenadas_flecha_gym == [135.0, 370.0]:
            numero = randint(0, 100)
            identificador = "client"
            socket_client({"tipo_mensaje": "inicio_lucha", "servidor": {"nombre": '', "numero": 0},
                           "cliente": {"nombre": nombre_jugador.get_entrenador(), "numero": numero}})


# Funcion que incluye sub funciones de que realizar si se presiona la tecla de retroseso s
def keys(event):
    global item_menu, menu_activo, ventana_principal, poke_list_team
    if item_menu:
        ventana_principal.delete("itembox")
        pokebola.destroy()
        cantidad_pokebola.destroy()
        pelea_2(pokemon_en_batalla)
        item_menu = False
    elif menu_activo:
        borrar_menu()
    elif poke_list_team:
        menu_activo = True
        ventana_principal.delete("itembox")
        borrar_label_pokemon(name_label_list)


# Funcion que incluye sub funciones de que realizar si se presiona la tecla enter
def key_enter(event):
    # variables globales para poder remover
    global nombre_texto, lblnombre, menu_activo, mensaje_oaak, mensaje, back_ident, nombre_jugador
    if back_ident == "prof_intro_4":
        back_ident = cambiar_background("prof_lab", ventana_principal, background_sprite, background)
        nombre_jugador = Entrenador(nombre_texto.get())
        # borrar entry
        nombre_texto.place_forget()
        # borrar label
        lblnombre.place_forget()
        pygame.mixer.music.stop()
        music = "music/oak_research_lab.mp3"
        pygame.mixer.music.load(music)
        pygame.mixer.music.play(-1)
        ventana_principal.coords("Red", coordenadas_red[0], coordenadas_red[1])
        ventana_principal.coords("pikachu", coordenadas_pikachu[0], coordenadas_pikachu[1])
        ventana_principal.create_image((285, 210), image=prof_oak, tag="oak")
        mensaje_oaak = True
        message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
        mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                + "llevarlo contigo siempre de ahora en adelante"
                        , font=("Pokemon R/B/Y", 8))
        mensaje.place(x=20, y=480)
        ventana_principal.focus_set()
    elif menu_activo:
        pass
    elif abrir_menu():
        menu()


# funcion utilizada para cambiar la posicion de red y colocar a pikachu a la derecha
# Entrada: el valor en x y el valor en y donde se va a ubicar a red
def reubicar_red(x, y):
    Red_sprite = PhotoImage(file="sprites/RED/Red_Front_0.png")
    pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_back_1.png")
    ventana_principal.coords("Red", x, y)
    ventana_principal.coords("pikachu", x + 25, y)


# funcion utilizada para mover a red
# Entrada: el cambio en x y en y
def move_red(x, y):
    global ventana_principal, background_sprite, background_aux, back_ident, background, Red, pikachu, music \
        , vida_wild_fija, servidor_estado, ganador_host, ganador_cliente
    # obtener las coordenadas actuales de red
    coordinates_red = ventana_principal.coords("Red")
    # calcular las nuevas coordenadas de red
    new_cordinates = (coordinates_red[0] + x, coordinates_red[1] + y)
    print(new_cordinates)
    # valida las coordenadas para evitar los obstaculos que estan enlistados de acuerdo al mapa
    if move_is_valid(new_cordinates):
        # si red esta en el laboratorio calcular las nuevas posiciones
        if back_ident == "prof_lab":
            # verificar si la nueva coordenada es afuera del laboratio
            if in_space(new_cordinates, lab_limite_afuera):
                # salirse del laboratorio (cambiar background a pueblo paleta)
                back_ident = cambiar_background("pallet_town", ventana_principal, background_sprite, background)
                # mover a red afuera del edificio del laboratorio
                reubicar_red(365, 385)
                pygame.mixer.music.stop()
                music = "music/palette_town_theme.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
                ventana_principal.delete("oak")

            else:
                # si no se sale del mapa mover a pikachu a la coordenada actual de red
                move_pikachu(coordinates_red)
                # mover a red a las nuevas coordenadas
                ventana_principal.move("Red", x, y)
        # si red esta en el pueblo paleta
        elif back_ident == "pallet_town":
            # si la nuva coordenada de la puerta
            if in_space(new_cordinates, puerta_lab):
                # moverse a dentro del laboratorio
                back_ident = cambiar_background("prof_lab", ventana_principal, background_sprite, background)
                # reubicar a red y a pikachu en la entrada
                reubicar_red(285, 485)
                pygame.mixer.music.stop()
                music = "music/oak_research_lab.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
                ventana_principal.create_image((285, 210), image=prof_oak, tag="oak")
            elif in_space(new_cordinates, bosque_1):
                back_ident = cambiar_background("ver_for_1", ventana_principal, background_sprite, background)
                reubicar_red(300, 500)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                pygame.mixer.music.stop()
                music = "music/viridian_forest.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
            elif in_space(new_cordinates, ganador_1):
                back_ident = cambiar_background("ganador", ventana_principal, background_sprite, background)
                ganador_host = True
                ganador()
            elif in_space(new_cordinates, ganador_2):
                back_ident = cambiar_background("ganador", ventana_principal, background_sprite, background)
                ganador_cliente = True
                ganador()
                # Elif del pueblo paleta hacia veridian forest_1


            else:
                # si no se sale del mapa mover a pikachu a la coordenada actual de red
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
        # Cambiar el siguiente ver_for por veridian_forest_1(la primera imagen del veridian_forest)
        elif back_ident == "ver_for_1":
            if in_space(new_cordinates, bosque_2):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_2", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(375.0, 300.0)
            elif in_space(new_cordinates, bosque_4):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_3", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(260.0, 550.0)
            elif in_space(new_cordinates, back_to_town):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("pallet_town", ventana_principal, background_sprite, background)
                reubicar_red(340.0, 60.0)
                pygame.mixer.music.stop()
                music = "music/palette_town_theme.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
            elif choca_wild_pokemon(new_cordinates, poke_list):
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            else:
                pass
                # agregar los if in_space que permiten ir a las otras partes del bosque(el in_space hace que se
                # salga del bosque)
        elif back_ident == "ver_for_2":
            if in_space(new_cordinates, bosque_3):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_1", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(50, 375)
            elif choca_wild_pokemon(new_cordinates, poke_list):
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            else:
                # agregar los if in_space que permiten ir a las otras partes del bosque(el in_space hace que se salga del bosque)
                pass
        elif back_ident == "ver_for_3":
            if in_space(new_cordinates, bosque_back_1):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_1", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(525, 375)
            elif in_space(new_cordinates, bosque_5):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_4", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(350, 475)
            elif choca_wild_pokemon(new_cordinates, poke_list):
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            else:
                pass
        elif back_ident == "ver_for_4":
            if in_space(new_cordinates, bosque_back_2):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_3", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(260, 50)
            elif in_space(new_cordinates, bosque_back_3):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_3", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(260, 50)
            elif in_space(new_cordinates, bosque_6):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_5", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(550.0, 75.0)
            elif choca_wild_pokemon(new_cordinates, poke_list):
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            else:
                pass
        elif back_ident == "ver_for_5":
            if in_space(new_cordinates, silver_town):
                delete_wild_pokemon(ventana_principal)
                pygame.mixer.music.stop()
                music = "music/silver_town.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(500, 575)
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            elif in_space(new_cordinates, bosque_back_6) or in_space(new_cordinates, bosque_back_7):
                delete_wild_pokemon(ventana_principal)
                back_ident = cambiar_background("ver_for_4", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(100, 475)
            elif choca_wild_pokemon(new_cordinates, poke_list):
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
        elif back_ident == "silver_town":
            if in_space(new_cordinates, bosque_5_de_ciudad):
                music = "music/viridian_forest.mp3"
                pygame.mixer.music.load(music)
                pygame.mixer.music.play(-1)
                back_ident = cambiar_background("ver_for_5", ventana_principal, background_sprite, background)
                vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
                draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
                reubicar_red(50.0, 25.0)
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            elif in_space(new_cordinates, ciudad_2):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(100.0, 575.0)
            elif in_space(new_cordinates, ciudad_2_camino_1):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(175.0, 575.0)
            elif in_space(new_cordinates, ciudad_2_monte):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(275.0, 575.0)
            elif in_space(new_cordinates, ciudad_2_monte_2):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(425.0, 575.0)
            elif in_space(new_cordinates, ciudad_2_camino_2):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(550.0, 575.0)
            elif in_space(new_cordinates, ciudad_3_camino_1):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_monte_1):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 150.0)
            elif in_space(new_cordinates, ciudad_3_monte_2):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 300.0)
            elif in_space(new_cordinates, ciudad_3_camino_2):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 425.0)
            elif in_space(new_cordinates, ciudad_3_monte_3):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 500.0)
            else:
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
        elif back_ident == "silver_town_1":
            if in_space(new_cordinates, ciudad_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(100.0, 25.0)
            elif in_space(new_cordinates, ciudad_1_camino_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(175.0, 25.0)
            elif in_space(new_cordinates, ciudad_1_monte):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(275.0, 25.0)
            elif in_space(new_cordinates, ciudad_1_monte_2):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(425.0, 25.0)
            elif in_space(new_cordinates, ciudad_1_camino_2):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(550.0, 25.0)
            elif in_space(new_cordinates, ciudad_4_camino_mitad):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 575.0)
            elif in_space(new_cordinates, ciudad_4_monte_1):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 525.0)
            elif in_space(new_cordinates, ciudad_4_camino_1):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 400.0)
            elif in_space(new_cordinates, entrar_gym):
                back_ident = cambiar_background("gym", ventana_principal, background_sprite, background)
                gym_menu()
                reubicar_red(-100, -100)
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
            elif in_space(new_cordinates, ciudad_4_monte_2):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 325.0)
            elif in_space(new_cordinates, ciudad_4_monte_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 200.0)
            elif in_space(new_cordinates, ciudad_4_monte_4):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(25.0, 100.0)
            elif in_space(new_cordinates, probar):
                back_ident = cambiar_background("inicio_batalla", ventana_principal, background_sprite, background)
                nombre_numero()
            else:
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
        elif back_ident == "silver_town_2":
            if in_space(new_cordinates, ciudad_3_camino_1_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_monte_1_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 150.0)
            elif in_space(new_cordinates, ciudad_3_monte_2_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 300.0)
            elif in_space(new_cordinates, ciudad_3_camino_2_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 425.0)
            elif in_space(new_cordinates, ciudad_3_monte_3_1):
                back_ident = cambiar_background("silver_town", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 500.0)
            elif in_space(new_cordinates, ciudad_4_camino_1_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(475.0, 575.0)
            elif in_space(new_cordinates, ciudad_4_camino_2_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(375.0, 575.0)
            elif in_space(new_cordinates, ciudad_4_camino_3_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(275.0, 575.0)
            elif in_space(new_cordinates, ciudad_4_camino_4_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(175.0, 575.0)
            elif in_space(new_cordinates, ciudad_4_camino_5_3):
                back_ident = cambiar_background("silver_town_3", ventana_principal, background_sprite, background)
                reubicar_red(75.0, 575.0)
            else:
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)
        elif back_ident == "silver_town_3":
            if in_space(new_cordinates, ciudad_2_monte_1_4):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 100.0)
            elif in_space(new_cordinates, ciudad_2_monte_2_4):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 200.0)
            elif in_space(new_cordinates, ciudad_2_monte_3_4):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 325.0)
            elif in_space(new_cordinates, ciudad_2_camino_1_4):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 400.0)
            elif in_space(new_cordinates, ciudad_2_monte_4_4):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 500.0)
            elif in_space(new_cordinates, ciudad_2_camino_mitad_1):
                back_ident = cambiar_background("silver_town_1", ventana_principal, background_sprite, background)
                reubicar_red(575.0, 575.0)
            elif in_space(new_cordinates, ciudad_3_camino_1_4):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(75.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_camino_2_4):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(175.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_camino_3_4):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(275.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_camino_4_4):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(375.0, 25.0)
            elif in_space(new_cordinates, ciudad_3_camino_5_4):
                back_ident = cambiar_background("silver_town_2", ventana_principal, background_sprite, background)
                reubicar_red(475.0, 25.0)
            else:
                move_pikachu(coordinates_red)
                ventana_principal.move("Red", x, y)


contador_arriba = 0


# Entrada: un contador local
# la funcion lo que hace es cambiar la imagen de red y pikachu cuando se dirige hacia arriba para que parezca estar en movimiento
def red_camina_arriba(cont_lo):
    global Red, Red_sprite, ventana_principal, contador_arriba, pikachu_sprite, pikachu
    cont_lo = contador_arriba
    if contador_arriba == 0:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Back_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_back_0.png")
        move_red(0, -25)
        contador_arriba += 1
    elif contador_arriba == 1:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Back_1.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_back_1.png")
        move_red(0, -25)
        contador_arriba += 1
    elif contador_arriba == 2:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Back_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_back_0.png")
        move_red(0, -25)
        contador_arriba += 1
    elif contador_arriba == 3:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Back_2.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_back_2.png")
        move_red(0, -25)
        contador_arriba = contador_arriba * 0
    ventana_principal.itemconfig(Red, image=Red_sprite, tag="Red")
    ventana_principal.itemconfig(pikachu, image=pikachu_sprite, tag="pikachu")


contador_abajo = 0


# Entrada: un contador local
# la funcion lo que hace es cambiar la imagen de red y pikachu cuando se dirige hacia abajo para que parezca estar en movimiento
def red_camina_abajo(cont_lo):
    global Red, Red_sprite, ventana_principal, contador_abajo, pikachu, pikachu_sprite
    cont_lo = contador_abajo
    if contador_abajo == 0:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Front_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_front_0.png")
        move_red(0, 25)
        contador_abajo += 1
    elif contador_abajo == 1:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Front_1.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_front_1.png")
        move_red(0, 25)
        contador_abajo += 1
    elif contador_abajo == 2:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Front_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_front_0.png")
        move_red(0, 25)
        contador_abajo += 1
    elif contador_abajo == 3:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Front_2.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_front_2.png")
        move_red(0, 25)
        contador_abajo = contador_abajo * 0
    ventana_principal.itemconfig(Red, image=Red_sprite, tag="Red")
    ventana_principal.itemconfig(pikachu, image=pikachu_sprite, tag="pikachu")


contador_derecha = 0


# Entrada: un contador local
# la funcion lo que hace es cambiar la imagen de red y pikachu cuando se dirige hacia la derecha para que parezca estar en movimiento
def red_camina_derecha(cont):
    global Red, Red_sprite, ventana_principal, contador_derecha, pikachu, pikachu_sprite
    cont = contador_derecha
    if contador_derecha == 0:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Rigth_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_right_0.png")
        move_red(25, 0)
        contador_derecha += 1
    elif contador_derecha == 1:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Rigth_2.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_right_1.png")
        move_red(25, 0)
        contador_derecha -= 1
    ventana_principal.itemconfig(Red, image=Red_sprite, tag="Red")
    ventana_principal.itemconfig(pikachu, image=pikachu_sprite, tag="pikachu")


contador_izquierda = 0


# Entrada: un contador local
# la funcion lo que hace es cambiar la imagen de red y pikachu cuando se dirige hacia la izquierda para que parezca estar en movimiento
def red_camina_izquierda(cont):
    global Red, Red_sprite, ventana_principal, contador_izquierda, pikachu, pikachu_sprite
    cont = contador_izquierda
    if contador_izquierda == 0:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Left_0.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_left_0.png")
        move_red(-25, 0)
        contador_izquierda += 1
    elif contador_izquierda == 1:
        Red_sprite = PhotoImage(file="sprites/RED/Red_Left_1.png")
        pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_left_1.png")
        move_red(-25, 0)
        contador_izquierda -= 1
    ventana_principal.itemconfig(Red, image=Red_sprite, tag="Red")
    ventana_principal.itemconfig(pikachu, image=pikachu_sprite, tag="pikachu")


# verificar si las coordenadas no se encuentran dentro de algun obstaculo segun el mapa actual
# Entrada: las coordenadas por verificar
def move_is_valid(new_cordinates):
    # si esta dentro del laboratorio
    if (back_ident == "prof_lab"):
        # verifica las coordenadas en screen_limits_list_lab
        if not check_list(new_cordinates, screen_limits_list_lab):
            return False
        else:
            return True
    # Si esta en el pueblo paleta
    elif (back_ident == "pallet_town"):
        # verificar las coordenadas en screen_limits_list_pallet
        if check_list(new_cordinates, screen_limits_list_pallet):
            return True
        else:
            return False
    elif (back_ident == "ver_for_1"):
        if check_list(new_cordinates, screen_limits_list_forest):
            return True
        else:
            return False

    elif (back_ident == "ver_for_2"):
        if check_list(new_cordinates, screen_limits_list_forest_2):
            return True
        else:
            return False

    elif (back_ident == "ver_for_3"):
        if check_list(new_cordinates, screen_limits_list_forest_3):
            return True
        else:
            return False
    elif (back_ident == "ver_for_4"):
        if check_list(new_cordinates, screen_limits_list_forest_4):
            return True
        else:
            return False
    elif (back_ident == "ver_for_5"):
        if check_list(new_cordinates, screen_limits_list_forest_5):
            return True
        else:
            return False
    elif (back_ident == "silver_town"):
        if check_list(new_cordinates, screen_limits_list_ciudad_1):
            return True
        else:
            return False
    elif (back_ident == "silver_town_1"):
        if check_list(new_cordinates, screen_limits_list_ciudad_2):
            return True
        else:
            return False
    elif (back_ident == "silver_town_2"):
        if check_list(new_cordinates, screen_limits_list_ciudad_3):
            return True
        else:
            return False
    elif (back_ident == "silver_town_3"):
        if check_list(new_cordinates, screen_limits_list_ciudad_4):
            return True
        else:
            return False
    elif (back_ident == "gym"):
        return True
        # si la pantalla no existe, devuelv falso
        # HACER elif back ident == veridian_forest_1
        # Hacer elif bakc ident == veridian forest_X


# Validar si una coordenada se encuentra se encuentra en alguno de los obtaculos espeficados en una lista
# Entrada:new cordinates son el punto a verificar en cada obstaculo
# entrada screen_limits_list es una lista de areas que representan un obstaculo especificado (x1, y1),(x2,y2)
def check_list(new_cordinates, screen_limits_list):
    if len(screen_limits_list) == 0:
        return True
    else:
        if not in_space(new_cordinates, screen_limits_list[0]):
            return check_list(new_cordinates, screen_limits_list[1:])
        else:
            return False


# verifica si el punto esta dentro de un area
# entrada point el punto a verificar dentro del area
# entrada limits area especificada de la forma (x1, y1),(x2,y2) donde x y y son los extremos de la diagonal de un rectangulo
def in_space(point, limits):
    if limits[0][0] <= point[0] <= limits[1][0] and limits[0][1] <= point[1] <= limits[1][1]:
        return True
    else:
        return False


# mover a pikachu a las nuevas coordenadas que son espeficiadas con parametro
# Entrada new_cordinates_pikachu las coordenadas de la forma (x,y)
def move_pikachu(new_cordinates_pikachu):
    last_coordinates_pikachu = ventana_principal.coords("pikachu")
    delta_x = new_cordinates_pikachu[0] - last_coordinates_pikachu[0]
    delta_y = new_cordinates_pikachu[1] - last_coordinates_pikachu[1]
    ventana_principal.move("pikachu", delta_x, delta_y)


# Funcion que realiza el guardado de los datos importantes, como la posicion de Red, de pikachu, la lista de pokemones
# en el equipo, y el identificador del background, en un archivo txt ubicado en la carpeta save, si el archivo no existe
# esta funcion tambien se encarga de crearlo
def save():
    global coordenadas_red, coordenadas_pikachu, back_ident, poke_team, ventana_principal, saved, nombre_jugador, nombre_jugador
    coordenadas_red = ventana_principal.coords("Red")
    coordenadas_pikachu = ventana_principal.coords("pikachu")
    file = open("save/save.txt", "wb")
    save ={"Coordenadas_red": coordenadas_red, "Coordenadas_pikachu" : [coordenadas_pikachu[0], coordenadas_pikachu[1]],
           "Back_ident": back_ident, "poke_team":poke_team, "Nombre": nombre_jugador}
    pickle.dump(save, file)
    file.close()
    saved = True



# Funcion utilizada para abrir y cargar los datos del archivo de guardado save.txt, este carga los datos nueveamente en
# las variables globales
def load_save():
    global coordenadas_red, coordenadas_pikachu, back_ident, poke_team, nombre_jugador
    file = open("save/save.txt", "rb")
    unsave = pickle.load(file)
    nombre_jugador = unsave["Nombre"]
    coordenadas_red = unsave["Coordenadas_red"]
    coordenadas_pikachu = unsave["Coordenadas_pikachu"]
    back_ident = unsave["Back_ident"]
    poke_team = unsave["poke_team"]
    file.close()


# Funcion recursiva que se encarga de cargar todos los pokemones en el equipo del archivo txt
def load_poke_team(file, poke_team):
    pokemon = file.readline().replace("\n", "")
    vida = file.readline().replace("\n", "")
    if pokemon == "stop":
        return True
    else:
        poke_team.append([pokemon, int(vida), int(vida)])
        return load_poke_team(file, poke_team)


# Funcion recursiva que se encarga de guardar todos los pokemones del equipo en el archivo txt
def save_poke_team(file, poke_team):
    if len(poke_team) == 0:
        return True
    else:
        file.write(str(poke_team[0][0]) + "\n")
        file.write(str(poke_team[0][2]) + "\n")
        return save_poke_team(file, poke_team[1:])


# Variables globales importantes utilizadas por una gran cantidad de funciones del programa, en breve se detalla su
# funcionalidad
global poke_list, poke_team, name_label_list, pokemon_en_batalla, item_menu, item_box, capturado, bosque_proviene \
    , vida_wild, poke_box, menu_activo, poke_list_team, perdio_batalla, saved, mensaje_fainted
global coordenadas_red, coordenadas_pikachu, sin_pokemon, atacando
ganador_cliente = False
ganador_host = False
poke_list = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]  # incializacio de la lista de pokemones random
# cuyo orden de valores es
#  [numero de pokemon, posicion x, posicion y]
name_label_list = []  # inicializacion de la  lsita para los label de los nombres de los pokemones en el equipo
poke_team = []  # inicializacion de la lista de los pokemones en el equipo
pokemon_en_batalla = 0  # variable utilizada para identificar que pokemon de la lista del equipo esa utilizado en batalla
capturado = 0  # variable que identifica cual de los 5 pokemones salvages fue capturado
ataca_enemigo = False  # variable que marca el ataque del pokemon enemigo
capturado_bool = False  # vairable que marca cuando se captura un pokemon
item_menu = False  # variable que marca el estado de activacion del menu de items
bosque_proviene = ""  # variable utilizada para almacenar el identificador del area del bosque proviniente antes de la
# batalla
coordenadas_red = [385, 260]  # Valores iniciales para las coordenadas de Red
coordenadas_pikachu = [410, 260]  # Valores iniciales para las coordenadas de Pikachu
vida_wild = 200  # Vida inicial de los pokemones salvages
vida_wild_fija = 200  # Vida maxima de todos los pokemones salvages, utilizada para calcular cuanto seria su 40%
vida_pokemon_fija = 200  # Vida maxima del pokemon del jugador
contador_ataques = 0  # Contador que indica cuantos atauqes lleva antes de dar el critico
poke_box = []  # Lista para guardar pokemones capturados cuando el equipo esta lleno
menu_activo = False  # Marca el estado del menu si este esta activo o inactivo
poke_list_team = False  # Marca si la lista de los nombres de los pokemones en el equipo aun se esta mostrando
perdio_batalla = False  # Marca si el jugador perdio la batalla al quedarse sin pokemones con los cuales combatir
saved = False  # Marca si el juego fue capaz de guardar con exito
mensaje_fainted = False  # Marca el estado del mensaje de un pokemon sin energia
gano_batalla = False  # Marca si el jugador gano la batalla pokemon
sin_pokemon = False  # Marca si el jugador se quedo sin pokemones con energia para batallar
mensaje_oaak = False  # Marca el estado del mensaje del profesor Oak
atacando = False
servidor_estado = False
estado_batalla = False

# Limites Laboratorio
estante_derecha = ((335.0, 305.0), (485.0, 360.0))
estante_izquierda = ((110.0, 305.0), (260.0, 365.0))
mesa_bola = ((335.0, 205.0), (460.0, 245.0))
basurero = ((460.0, 210.0), (485.0, 235.0))
estante_superior = ((335.0, 85.0), (520.0, 165.0))
mesa_superior = ((80.0, 125.0), (260.0, 160.0))
pared1_abajo = ((110.0, 510.0), (235.0, 510.0))
pared2_abajo = ((335.0, 510.0), (485.0, 510.0))
pared_izquierda = ((20.0, 85.0), (100.0, 485.0))
pared_derecha = ((500.0, 85.0), (580.0, 485.0))
pared_arriba = ((60.0, 5.0), (520.0, 125.0))
prof_oak_limit = ((285, 210), (285, 210))

lab_limite_afuera = ((285.0, 510), (310.0, 600))
puerta_lab = ((365.0, 360.0), (370.0, 360.0))
screen_limits_list_lab = [estante_derecha, estante_izquierda, mesa_bola, basurero, estante_superior, mesa_superior,
                          pared1_abajo, pared_derecha, pared_izquierda, pared_arriba, pared2_abajo, prof_oak_limit]
# limites pallet town
casa1_limits = ((140.0, 125.0), (240.0, 205.0))
rotulo1_limits = ((100, 185), (140, 205))
rotulo2_limits = ((320.0, 185.0), (360.0, 205.0))
casa2_limits = ((360.0, 125.0), (465.0, 205.0))
casa3_limits_a = ((315.0, 260.0), (465.0, 335.0))
casa3_limits_b = ((315.0, 360.0), (340.0, 360.0))
casa3_limits_c = ((390.0, 360.0), (465.0, 360.0))
baranda1_limits = ((300.0, 405.0), (465.0, 425.0))
baranda2_limits = ((140.0, 305.0), (240.0, 310.0))
laguna_limits = ((140.0, 425.0), (240.0, 585.0))
piedras_derecha_limits = ((560.0, 85.0), (600.0, 545.0))
piedras1_abajo_limits = ((240.0, 505.0), (580.0, 565.0))
piedras2_abajo_limits = ((60.0, 505.0), (80.0, 565.0))
piedras_izquierda_limits = ((0.0, 45.0), (40.0, 505.0))
piedras3_abajo_limits = ((90.0, 535.0), (120.0, 565.0))
piedras1_arriba_limits = ((40.0, -15.0), (300.0, 85.0))
piedras2_arriba_limits = ((360.0, -15.0), (540.0, 85.0))
ganador_1 = ((115.0, 210.0), (115.0, 210.0))
ganador_2 =((340.0, 210.0), (340.0, 210.0))
screen_limits_list_pallet = [rotulo1_limits, casa1_limits, rotulo2_limits, \
                             casa2_limits, casa3_limits_a, baranda1_limits, baranda2_limits, laguna_limits,
                             piedras_derecha_limits, \
                             piedras1_abajo_limits, piedras2_abajo_limits, piedras_izquierda_limits,
                             piedras3_abajo_limits, \
                             piedras1_arriba_limits, piedras2_arriba_limits, casa3_limits_b, casa3_limits_c]

# Limites Bosque
back_to_town = ((250.0, 525.0), (375.0, 525.0))
bosque_inferior_izquierda = ((0.0, 450.0), (225.0, 550.0))
bosque_inferior_derecha = ((375.0, 450.0), (600.0, 525.0))
letrero_inferior = ((350.0, 450.0), (375.0, 500.0))
letrero_superior_1 = ((550.0, 325.0), (575.0, 350.0))
letrero_superior_2 = ((275.0, 100.0), (300.0, 125.0))
bosque_superior_izquierda = ((50.0, 150.0), (225.0, 325.0))
bosque_superior_derecha = ((375.0, 50.0), (550.0, 325.0))
limite_superior = ((25.0, 50.0), (575.0, 100.0))
arbol_central = ((275.0, 350.0), (325.0, 400.0))
screen_limits_list_forest = [bosque_inferior_izquierda, bosque_inferior_derecha, letrero_inferior, letrero_superior_1,
                             letrero_superior_2, bosque_superior_izquierda, bosque_superior_derecha, limite_superior,
                             arbol_central]
bosque_1 = [(315.0, 35.0), (340.0, 35.0)]
bosque_2 = [(25.0, 100.0), (25.0, 425.0)]
# Limites Bosque 2
bosque_inferior_2 = ((175.0, 475.0), (425.0, 575.0))
bosque_superior_2 = (((175.0, 25.0), (425.0, 100.0)))
bosque_central_2 = ((175.0, 175.0), (350.0, 375.0))
fila_troncos_1_2 = ((200.0, 125.0), (200.0, 150.0))
fila_troncos_2_2 = ((200.0, 375.0), (200.0, 450.0))

screen_limits_list_forest_2 = [bosque_1, bosque_2, bosque_inferior_2, bosque_superior_2, bosque_central_2,
                               fila_troncos_1_2, fila_troncos_2_2]
bosque_3 = [(400.0, 125.0), (400.0, 450.0)]

# Limites Bosque 3
fila_troncos_1_3 = ((210.0, 25.0), (210.0, 575.0))
fila_troncos_2_3 = ((460.0, 25.0), (460.0, 400.0))
bosque_inferior_derecha_3 = ((310.0, 425.0), (460.0, 550.0))
bosque_central_3 = ((310.0, 50.0), (385.0, 350.0))
bosque_back_1 = ((235.0, 575.0), (310.0, 575.0))
bosque_5 = ((235.0, 25.0), (460.0, 25.0))
screen_limits_list_forest_3 = [fila_troncos_2_3, fila_troncos_1_3, bosque_inferior_derecha_3, bosque_central_3]
bosque_4 = [(575.0, 350.0), (575.0, 425.0)]

# Limites bosque 4
bosque_inferior_derecha_4 = ((400.0, 350.0), (475.0, 475.0))
tramo_troncos_derecha_4 = ((600.0, 100.0), (600.0, 475.0))
tramo_troncos_superior_4 = ((600.0, 100.0), (50.0, 100.0))
bloque_inferior_izquierda_4 = ((175.0, 350.0), (325.0, 500.0))
bosque_centra_4 = ((175.0, 175.0), (475.0, 275.0))
tramo_arboles_4 = ((0.0, 100.0), (50.0, 500.0))
bosque_back_2 = [(350.0, 475.0), (375.0, 475.0)]
bosque_back_3 = ((500.0, 500.0), (575.0, 500.0))
tramo_troncos_centro_4 = ((500.0, 200.0), (500.0, 275.0))
bosque_6 = ((75.0, 500.0), (150.0, 500.0))
screen_limits_list_forest_4 = [bosque_inferior_derecha_4, tramo_troncos_derecha_4, tramo_troncos_superior_4,
                               bloque_inferior_izquierda_4, bosque_centra_4, tramo_arboles_4, tramo_troncos_centro_4]

bosque_back_4 = ((575.0, 25.0), (575.0, 75.0))

# Limites bosque 5
bosque_back_5 = ((600.0, 200.0), (600.0, 225.0))
tramo_troncos_superior_5 = ((175.0, 0.0), (600.0, 0.0))
tramo_troncos_derecha_1_5 = ((600.0, 175.0), (600.0, 100.0))
tramo_troncos_derecha_2_5 = ((600.0, 250.0), (600.0, 500.0))
bosque_inferior_derecha_5 = ((325.0, 525.0), (575.0, 600.0))
fila_bosque_central_5 = ((450.0, 25.0), (475.0, 400.0))
tramo_troncos_centro_5 = ((275.0, 100.0), (325.0, 600.0))
troncos_y_bosque_derecha = ((100.0, 25.0), (175.0, 550.0))
tramo_troncos_izquierda_5 = ((25.0, 50.0), (25.0, 575.0))
letrero_5 = ((75.0, 25.0), (75.0, 25.0))
limite_inferior_5 = ((25.0, 600.0), (275.0, 600.0))
bosque_back_6 = ((600.0, 200.0), (600.0, 225.0))
bosque_back_7 = ((600.0, 25.0), (600.0, 75.0))
silver_town = ((50.0, 25.0), (75.0, 50.0))
screen_limits_list_forest_5 = [tramo_troncos_superior_5, tramo_troncos_derecha_1_5, tramo_troncos_derecha_2_5,
                               bosque_inferior_derecha_5, fila_bosque_central_5, tramo_troncos_centro_5,
                               troncos_y_bosque_derecha, tramo_troncos_izquierda_5, letrero_5, limite_inferior_5]
# limites ciudad platenada 1
bosque_abajo_ciudad_1 = ((50.0, 550.0), (425.0, 600.0))
bosque_izquierda_ciudad_1 = ((25.0, 25.0), (25.0, 550.0))
bosque_abajo_2_ciudad_1 = ((575.0, 550.0), (575.0, 575.0))
casa_1_ciudad_1 = ((125.0, 350.0), (225.0, 425.0))
centro_pokemon = ((250.0, 50.0), (475.0, 175.0))
bosque_5_de_ciudad = ((450.0, 600.0), (550.0, 600.0))
ciudad_2 = ((50.0, 0.0), (125.0, 0.0))
ciudad_2_camino_1 = ((75.0, 0.0), (200.0, 0.0))
ciudad_2_monte = ((225.0, 0.0), (350.0, 0.0))
ciudad_2_monte_2 = ((375.0, 0.0), (525.0, 0.0))
ciudad_2_camino_2 = ((525.0, 0.0), (600.0, 0.0))
ciudad_3_camino_1 = ((600.0, 25.0), (600.0, 25.0))
ciudad_3_monte_1 = ((600.0, 50.0), (600.0, 200.0))
ciudad_3_monte_2 = ((600.0, 225.0), (600.0, 375.0))
ciudad_3_camino_2 = ((600.0, 400.0), (600.0, 425.0))
ciudad_3_monte_3 = ((600.0, 450.0), (600.0, 525.0))
screen_limits_list_ciudad_1 = [bosque_abajo_ciudad_1, bosque_izquierda_ciudad_1, bosque_abajo_2_ciudad_1, \
                               casa_1_ciudad_1, centro_pokemon]

# Limites ciudad plateada 2
arbol_1_1 = ((150.0, 375.0), (175.0, 500.0))
cartel = ((200.0, 475.0), (225.0, 500.0))
gym = ((225.0, 375.0), (300.0, 475.0))
gym_2 = ((350.0, 375.0), (400.0, 475.0))
gym_3 = ((225.0, 375.0), (400.0, 400.0))
arbol_2_1 = ((475.0, 375.0), (500.0, 475.0))
mujer = ((75.0, 450.0), (75.0, 475.0))
monta_1 = ((25.0, 250.0), (75.0, 250.0))
monta_2 = ((150.0, 250.0), (600.0, 250.0))
pared_izquierda_arboles = ((0.0, 300.0), (50.0, 600.0))
pared_izquierda = ((-25.0, 50.0), (0.0, 275.0))
pared_arriba_arboles = ((0.0, 0.0), (600.0, 50))
arbol_3_1 = ((175.0, 150.0), (225.0, 200.0))
arbol_4_1 = ((250.0, 125.0), (250.0, 175.0))
arbol_5_1 = ((250.0, 150.0), (275.0, 200.0))
arbol_6_1 = ((325.0, 125.0), (325.0, 175.0))
arbol_7_1 = ((350.0, 150.0), (350.0, 175.0))
arbol_8_1 = ((375.0, 125.0), (400.0, 150.0))
arbol_9_1 = ((400.0, 150.0), (425.0, 175.0))
arbol_10_1 = ((425.0, 125.0), (600.0, 175.0))
ciudad_1 = ((75.0, 600), (125.0, 600))
ciudad_1_camino_1 = ((90.0, 600), (200.0, 600.0))
ciudad_1_monte = ((225.0, 600.0), (350.0, 600.0))
ciudad_1_monte_2 = ((375.0, 600.0), (500.0, 600.0))
ciudad_1_camino_2 = ((525.0, 600.0), (600.0, 600.0))
ciudad_4_camino_mitad = ((600.0, 575.0), (600.0, 575.0))
ciudad_4_monte_1 = ((600.0, 450.0), (600.0, 550.0))
ciudad_4_camino_1 = ((600.0, 400.0), (600.0, 425.0))
ciudad_4_monte_2 = ((600.0, 275.0), (600.0, 375))
ciudad_4_monte_3 = ((600.0, 200.0), (600.0, 225))
ciudad_4_monte_4 = ((600.0, 75.0), (600, 100.0))
entrar_gym = ((325.0, 475.0), (325.0, 475.0))
probar = ((550.0, 275.0), (550.0, 275.0))
screen_limits_list_ciudad_2 = [arbol_1_1, cartel, gym, arbol_2_1, mujer, monta_1, monta_2, pared_izquierda_arboles, \
                               pared_izquierda, pared_arriba_arboles, arbol_3_1, arbol_4_1, arbol_5_1, arbol_6_1, \
                               arbol_7_1, arbol_8_1, arbol_9_1, arbol_10_1, gym_2, gym_3]

# Limites ciudad plateada 3
ciudad_3_camino_1_1 = ((0.0, 0.0), (0.0, 50.0))
ciudad_3_monte_1_1 = ((0.0, 75.0), (0.0, 225.0))
ciudad_3_monte_2_1 = ((0.0, 250.0), (0.0, 375.0))
ciudad_3_camino_2_1 = ((0.0, 400.0), (0.0, 425.0))
ciudad_3_monte_3_1 = ((0.0, 450.0), (0.0, 525.0))
ciudad_4_camino_1_3 = ((450.0, 0.0), (525.0, 0.0))
ciudad_4_camino_2_3 = ((325.0, 0.0), (425.0, 0.0))
ciudad_4_camino_3_3 = ((225.0, 0.0), (300.0, 0.0))
ciudad_4_camino_4_3 = ((125.0, 0.0), (200.0, 0.0))
ciudad_4_camino_5_3 = ((0.0, 0.0), (100, 0.0))
arboles_ciudad_3 = ((50.0, 75.0), (375.0, 150.0))
baranda_ciudad_3_1 = ((50.0, 175.0), (425.0, 200.0))
baranda_ciudad_3_2 = ((50.0, 175.0), (75.0, 375.0))
baranda_ciudad_3_3 = ((50.0, 325.0), (175.0, 375.0))
baranda_ciudad_3_4 = ((275.0, 325.0), (425.0, 375.0))
baranda_ciudad_3_5 = ((375.0, 175.0), (425.0, 375.0))
baranda_ciudad_3_6 = ((200.0, 375.0), (200.0, 375.0))
baranda_ciudad_3_7 = ((250.0, 375.0), (250.0, 375.0))
arboles_ciudad_3_abajo = ((25.0, 550.0), (575.0, 600.0))
pared_derecha_ciudad_3 = ((600.0, 0.0), (650.0, 550.0))
pared_arriba_ciudad_3 = ((550.0, 0.0), (650.0, 0.0))

screen_limits_list_ciudad_3 = [arboles_ciudad_3, baranda_ciudad_3_1, baranda_ciudad_3_2, baranda_ciudad_3_3, \
                               baranda_ciudad_3_4, baranda_ciudad_3_5, baranda_ciudad_3_6, baranda_ciudad_3_7, \
                               arboles_ciudad_3_abajo, pared_derecha_ciudad_3, pared_arriba_ciudad_3]
# Limites ciudad plateada 4
ciudad_2_monte_1_4 = ((0.0, 75.0), (0.0, 100.0))
ciudad_2_monte_2_4 = ((0.0, 200.0), (0.0, 250.0))
ciudad_2_monte_3_4 = ((0.0, 275.0), (0.0, 350.0))
ciudad_2_camino_1_4 = ((0.0, 375.0), (0.0, 425.0))
ciudad_2_monte_4_4 = ((0.0, 450.0), (0.0, 550.0))
ciudad_2_camino_mitad_1 = ((0.0, 575.0), (0.0, 575.0))
ciudad_3_camino_1_4 = ((0.0, 600.0), (100.0, 600.0))
ciudad_3_camino_2_4 = ((125.0, 600.0), (200.0, 600.0))
ciudad_3_camino_3_4 = ((225.0, 600.0), (300.0, 600.0))
ciudad_3_camino_4_4 = ((325.0, 600.0), (425.0, 600.0))
ciudad_3_camino_5_4 = ((450.0, 600.0), (525.0, 600.0))

arboles_arriba_ciudad_4 = ((25.0, 25.0), (550.0, 50.0))
arboles_derecha_ciudad_4 = ((475.0, 75.0), (575.0, 175.0))
arboles_derecha_ciudad_4_1 = ((550.0, 150.0), (600.0, 575.0))
casa_ciudad_4 = ((250.0, 275.0), (425.0, 350.0))
farmacia_ciudad_4 = ((50.0, 475.0), (125.0, 550.0))
arbol_solo_ciudad_4 = ((200.0, 500.0), (225.0, 550.0))
mae_ciudad_4 = ((300.0, 525, 0), (300.0, 525.0))
monta_ciudad_4 = ((0.0, 250.0), (425.0, 250.0))
arbol_1_4 = ((200.0, 125.0), (200.0, 150.0))
arbol_2_4 = ((75.0, 150.0), (175.0, 175.0))
arbol_3_4 = ((125.0, 175.0), (125.0, 200.0))
arbol_4_4 = ((75.0, 125.0), (100.0, 125.0))
arbol_5_4 = ((0.0, 150.0), (25.0, 175.0))
arbol_6_4 = ((50.0, 175.0), (50.0, 200.0))

screen_limits_list_ciudad_4 = [arboles_arriba_ciudad_4, arboles_derecha_ciudad_4, arboles_derecha_ciudad_4_1, \
                               casa_ciudad_4, farmacia_ciudad_4, arbol_solo_ciudad_4, mae_ciudad_4, \
                               monta_ciudad_4, arbol_1_4, arbol_2_4, arbol_3_4, arbol_4_4, arbol_5_4, arbol_6_4]

# Lista de nombres de pokemones
nombres_pokemon = ["Bulbasaur", "Ivysaur", "Venusaur", "Charmander", "Charmeleon", "Charizard", "Squirtle", "Wartortle",
                   "Blastoise", "Caterpie", "Metapod", "Butterfree", "Weedle", "Kakuna", "Beedrill", "Pidgey",
                   "Pidgeotto", "Pidgeot", "Rattata", "Raticate", "Spearow", "Fearow", "Ekans", "Arbok", "Pikachu",
                   "Raichu", "Sandshrew", "Sandslash", "Nidoran♀", "Nidorina", "Nidoqueen", "Nidoran♂", "Nidorino",
                   "Nidoking", "Clefairy", "Clefable", "Vulpix", "Ninetales", "Jigglypuff", "Wigglytuff", "Zubat",
                   "Golbat", "Oddish", "Gloom", "Vileplume", "Paras", "Parasect", "Venonat", "Venomoth", "Diglett",
                   "Dugtrio", "Meowth", "Persian", "Psyduck", "Golduck", "Mankey", "Primeape", "Growlithe", "Arcanine",
                   "Poliwag", "Poliwhirl", "Poliwrath", "Abra", "Kadabra", "Alakazam", "Machop", "Machoke", "Machamp",
                   "Bellsprout", "Weepinbell", "Victreebel", "Tentacool", "Tentacruel", "Geodude", "Graveler", "Golem",
                   "Ponyta", "Rapidash", "Slowpoke", "Slowbro", "Magnemite", "Magneton", "Farfetch'd", "Doduo",
                   "Dodrio", "Seel", "Dewgong", "Grimer", "Muk", "Shellder", "Cloyster", "Gastly", "Haunter", "Gengar",
                   "Onix", "Drowzee", "Hypno", "Krabby", "Kingler", "Voltorb", "Electrode", "Exeggcute", "Exeggutor",
                   "Cubone", "Marowak", "Hitmonlee", "Hitmonchan", "Lickitung", "Koffing", "Weezing", "Rhyhorn",
                   "Rhydon", "Chansey", "Tangela", "Kangaskhan", "	Horsea", "Seadra", "Goldeen", "Seaking", "Staryu",
                   "Starmie", "Mr. Mime", "Scyther", "Jynx", "Electabuzz", "Magmar", "Pinsir", "Tauros", "Magikarp",
                   "Gyarados", "Lapras", "Ditto", "Eevee", "Vaporeon", "Jolteon", "Flareon", "Porygon", "Omanyte",
                   "Omastar", "Kabuto", "Kabutops", "Aerodactyl", "Snorlax", "Articuno", "Zapdos", "Moltres", "Dratini",
                   "Dragonair", "Dragonite", "Mewtwo", "Mew"]

global background_sprite, background_aux, back_ident, background, Red, pikachu, message_box, save_encontrado, \
    file, prof_oak, thread_socket_servidor, thread_socket_cliente, recibido, numero, identificador, tablero, \
    lista_sprites_p1, lista_sprites_p2, primer_mensaje, tablero_activo, mi_turno
tablero_activo = False
primer_mensaje = False
tablero = False
identificador = ""
recibido = False
mi_turno = True
thread_socket_servidor = threading.Thread(target=servidor)
thread_socket_cliente = threading.Thread(target=cliente)
thread_server_2 = threading.Thread(target=socket_server_2)
thread_music = threading.Thread(target=play_music)
back_ident = "prof_intro_1"  # Variable que identifica el background de la pantalla
cont_dir = 0
save_encontrado = False  # Variable que indica si se logro encontrar el save
lista_sprites_p1 = []
lista_sprites_p2 = []
matriz_tablero = [[0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0]]
music = "music/opening.mp3"  # Inicializacion del archivo de musica
main = Tk()
pygame.init()
pygame.mixer.init()
pygame.font.init()
main.resizable(width=FALSE, height=FALSE)
main.wm_title("Poketec")
# Comienza la carga de Sprites importantes para el juego
item_box = PhotoImage(file="sprites/ecenarios/item_window.png")
background_sprite = PhotoImage(file="sprites/ecenarios/prof_oak_intro_1.png")
ventana_principal = Canvas(main, width=600, height=600)
Red_sprite = PhotoImage(file="sprites/RED/Red_Front_0.png")
pikachu_sprite = PhotoImage(file="sprites/pikachu/pika_front_0.png")
wild_sprite_on_world = PhotoImage(file="sprites/Wild_pokemon/Wild_pokemon_1.png")
flecha_seleccion = PhotoImage(file="sprites/ecenarios/flecha_opcion.png")
pokeball_2 = PhotoImage(file="sprites/ecenarios/pokeball_2.png")
attack = PhotoImage(file="sprites/ecenarios/attack.png")
attack_2 = PhotoImage(file="sprites/ecenarios/attack_2.png")
message_box = PhotoImage(file="sprites/ecenarios/catch_message.png")
menu_sprite = PhotoImage(file="sprites/ecenarios/menu.png")
prof_oak = PhotoImage(file="sprites/ecenarios/oak.png")
menu_gym = False
nombre = StringVar()
nombre_2 = nombre.get()
# Intenta cargar el archivo de save si encuentra el archivo marca el indicador como True, si el archivo no existe marca
# el indicador como False
try:
    load_save()
    save_encontrado = True
except FileNotFoundError:
    save_encontrado = False

# Si el save es encontrado y las variables son cargadas del archivo, este carga el estado en el que se realizo el save
# del juego
if save_encontrado:
    background = ventana_principal.create_image(300, 300, image=background_sprite)
    back_ident = cambiar_background(back_ident, ventana_principal, background_sprite, background)
    Red = ventana_principal.create_image((coordenadas_red[0], coordenadas_red[1]), image=Red_sprite, tag="Red")
    pikachu = ventana_principal.create_image((coordenadas_pikachu[0], coordenadas_pikachu[1]), image=pikachu_sprite,
                                             tag="pikachu")
    # Carga la musica dependiendo del area en el que se va a iniciar de acuerdo con el save
    if back_ident == "ver_for_1":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/viridian_forest.mp3"
    elif back_ident == "ver_for_3":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/viridian_forest.mp3"
    elif back_ident == "ver_for_4":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/viridian_forest.mp3"
    elif back_ident == "ver_for_5":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/viridian_forest.mp3"
    elif back_ident == "silver_town":
        music = "music/silver_town.mp3"
    elif back_ident == "silver_town_1":
        music = "music/silver_town.mp3"
    elif back_ident == "silver_town_2":
        music = "music/silver_town.mp3"
    elif back_ident == "silver_town_3":
        music = "music/silver_town.mp3"
    elif back_ident == "prof_lab":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/oak_research_lab.mp3"
        ventana_principal.create_image((285, 210), image=prof_oak, tag="oak")
        mensaje_oaak = True
        message_box_one = ventana_principal.create_image((300, 500), image=message_box, tag="message_oak")
        mensaje = Label(ventana_principal, text="Parece que a Pikachu le desagradan las pokebolas, deberas\n "
                                                + "llevarlo contigo siempre de ahora en adelante"
                        , font=("Pokemon R/B/Y", 8))
        mensaje.place(x=20, y=480)
    elif back_ident == "pallet_town":
        vida_wild_fija = set_pokemon(poke_list, 0, back_ident)
        draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world)
        music = "music/palette_town_theme.mp3"
    pygame.mixer.music.load(music)
    pygame.mixer.music.play(-1)
# En el caso en el que el save no exista o no se pueda cargar, el juego carga los datos para la intro del juego
else:
    pygame.mixer.music.load(music)
    pygame.mixer.music.play(-1)
    poke_team = [[25, 200, 200]]
    background = ventana_principal.create_image(300, 300, image=background_sprite)
    back_ident = cambiar_background(back_ident, ventana_principal, background_sprite, background)
    Red = ventana_principal.create_image((-10, -10), image=Red_sprite, tag="Red")
    pikachu = ventana_principal.create_image((-10, -10), image=pikachu_sprite,
                                             tag="pikachu")
# Espera a que alguna de los eventos del teclado sea realizado

ventana_principal.bind("<Up>", keyup)
ventana_principal.bind("<Left>", keyleft)
ventana_principal.bind("<Right>", keyright)
ventana_principal.bind("<Down>", keydown)
ventana_principal.bind("<a>", keya)
ventana_principal.bind("<Return>", key_enter)
ventana_principal.bind("<s>", keys)
# Esta funcion le da la prioridad al canvas de la ventana_principal
ventana_principal.focus_set()
ventana_principal.pack()
# Repite el loop principal de todo el juego
def main_poke():
    main.mainloop()