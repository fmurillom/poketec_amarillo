
class Entrenador:
    def __init__(self, nombre):
        self.nombre = nombre

    def get_entrenador(self):
        return self.nombre

    def set_entrenador(self, nombre):
        self.nombre = nombre

    def __str__(self):
        return " Nombre: " + self.get_entrenador()



