import base64
import pickle

def pokedex_list(lista, nombre):
    '''
    Anade el nombre del pokemon capturado a la lista de nombres
    :param lista: lista en la cual agregar
    :param nombre: nombre de pokemon a agregar
    :return:
    '''
    lista.append(nombre)

def crear_string_pokemon(lista):
    '''
    Crea un string con todas las imagenes
    :param lista: toma la lista de imagenes
    :return: retorna un string con todas las imagenes
    '''
    string = ""
    for i in lista:
        if i == lista[len(lista) - 1]:
            string += i
        else:
            string += i + ","
    return string

def convertir_base64(lista_nombres):
    '''
    Convierte una imagen a base64
    :param lista_nombres: lista de nombres disponibles
    :return: retorna una lista con todas las imagenes convertidas
    '''
    lista_convertidos = []
    for i in lista_nombres:
        with open(("Pokemon/" + i + ".jpg"), "rb") as image:
            string_64 = base64.b64encode(image.read())
            lista_convertidos.append(string_64)
    return lista_convertidos

def save_log(lista_pokedex):
    '''
    Guarda un registro de los pokemoes en el servidor
    :param lista_pokedex: lista de pokemones disponibles
    '''
    file = open("Pokemon/log.txt", "wb")
    pickle.dump(lista_pokedex, file)
    file.close()


def load_log():
    '''
    Carga un registro de los pokemones en el servidor
    :return: retorna la lista con los nombres disponibles
    '''
    lista = []
    try:
        file = open("Pokemon/log.txt", "rb")
        lista = pickle.load(file)
        file.close()
    except (FileNotFoundError):
        pass
    return lista