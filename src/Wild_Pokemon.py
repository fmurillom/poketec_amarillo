from random import randint
from tkinter import *

# Esta funcion se encarga de calcular aleatoriamente las coordenada de lo pokemon, asi como tambien calcula
# aleatoriamente el numero de la pokedex que repreneta el pokemon que aparecera
# Entradas: poke_list, lista para sobreeseribir con los datos de los nuevos pokemones
# contador para realizar la recurividad 5 veces para obtener 6 pokemones distintos
def set_pokemon(poke_list, contador, back_ident):
    vida_wild_fija = randint(100, 300)
    if contador == len(poke_list):
        return vida_wild_fija
    else:
        if back_ident == "ver_for_1" and contador == 0:  # Limites (50.0, 365.0) (250.0, 340.0)
            x = randint(50, 250)
            y = randint(340, 365)
            x = x - (x % 25)
            y = y - (y % 25)
            poke_number = randint(1, 151)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_1" and contador == 1:  # Limites (250.0, 315.0) (250.0, 165.0)
            x = 250
            y = randint(165, 315)
            y = y - (y % 25)
            poke_number = randint(1, 151)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_1" and contador == 2:  # Limites (50.0, 140.0) (275.0, 115.0)
            x = randint(50, 275)
            x = x - (x % 25)
            y = randint(115, 140)
            y = y - (y % 25)
            poke_number = randint(1, 151)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_1" and contador == 3:  # Limites (350.0, 365.0) (550.0, 340.0)
            x = randint(350, 550)
            x = x - (x % 25)
            y = randint(340, 365)
            y = y - (y % 25)
            poke_number = randint(1, 151)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_1" and contador == 4:  # Limites (350.0, 300.0) (350.0, 100.0)
            poke_number = randint(1, 151)
            x = 350.0
            y = randint(100, 300)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_2" and contador == 0:  # Limites (225.0, 475.0) (350.0, 375.0)
            poke_number = randint(1, 151)
            x = randint(225, 350)
            y = randint(375, 475)
            x = x - (x % 25)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_2" and contador == 1:  # Limites (225.0, 150.0) (375.0, 125.0)
            poke_number = randint(1, 151)
            x = randint(225, 375)
            y = randint(125, 150)
            x = x - (x % 25)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_2" and contador == 2:  # Limites (225.0, 475.0) (350.0, 375.0)
            poke_number = randint(1, 151)
            x = randint(225, 350)
            y = randint(375, 475)
            x = x - (x % 25)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_2" and contador == 3:  # Limites (235.0, 350.0) (285.0, 75.0)
            poke_list[contador] = [0, -10, -10]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_2" and contador == 4:  # Limites (235.0, 350.0) (285.0, 75.0)
            poke_list[contador] = [0, -10, -10]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_3" and contador == 0:  # Limites (235.0, 550.0) (235.0, 350.0)
            poke_number = randint(1, 151)
            x = 235.0
            y = randint(350, 550)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_3" and contador == 1:  # Limites (235.0, 350.0) (285.0, 75.0)
            poke_number = randint(1, 151)
            x = randint(235, 285)
            x = x - (x % 25) + 10
            y = randint(75, 350)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_3" and contador == 2:
            poke_number = randint(1, 151)
            x = randint(235, 285)
            x = x - (x % 25) + 10
            y = randint(75, 350)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_3" and contador == 3:  # Limites (235.0, 350.0) (285.0, 75.0)
            poke_list[contador] = [0, -10, -10]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_3" and contador == 4:  # Limites (235.0, 350.0) (285.0, 75.0)
            poke_list[contador] = [0, -10, -10]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_4" and contador == 0:  # Limites (500.0, 475.0) (575.0, 350.0)
            poke_number = randint(1, 151)
            x = randint(500, 575)
            x = x - (x % 25)
            y = randint(350, 475)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_4" and contador == 1:  # Limites (175.0, 325.0) (300.0, 300.0)
            poke_number = randint(1, 151)
            x = randint(175, 300)
            x = x - (x % 25)
            y = randint(300, 325)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_4" and contador == 2:  # Limites  (75.0, 475.0) (150.0, 250.0)
            poke_number = randint(1, 151)
            x = randint(75, 150)
            x = x - (x % 25)
            y = randint(250, 475)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_4" and contador == 3:  # Limites (500.0, 475.0) (575.0, 350.0)
            poke_number = randint(1, 151)
            x = randint(500, 575)
            x = x - (x % 25)
            y = randint(350, 475)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_4" and contador == 4:  # Limites  (75.0, 475.0) (150.0, 250.0)
            poke_number = randint(1, 151)
            x = randint(75, 150)
            x = x - (x % 25)
            y = randint(250, 475)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_5" and contador == 0:  # Limites (500.0, 450.0)  (575.0, 150.0)
            poke_number = randint(1, 151)
            x = randint(500, 575)
            x = x - (x % 25)
            y = randint(150, 450)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_5" and contador == 1:  # Limites (325.0, 500.0) (450.0, 150.0)
            poke_number = randint(1, 151)
            x = randint(325, 450)
            x = x - (x % 25)
            y = randint(150, 500)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_5" and contador == 2:  # Limites (200.0, 575.0) (275.0, 150.0)
            poke_number = randint(1, 151)
            x = randint(200, 275)
            x = x - (x % 25)
            y = randint(150, 575)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_5" and contador == 3:  # Limites (25.0, 575.0) (75.0, 150.0)
            poke_number = randint(1, 151)
            x = randint(25, 75)
            x = x - (x % 25)
            y = randint(150, 575)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)
        elif back_ident == "ver_for_5" and contador == 4:  # Limites (500.0, 450.0)  (575.0, 150.0)
            poke_number = randint(1, 151)
            x = randint(200, 275)
            x = x - (x % 25)
            y = randint(150, 575)
            y = y - (y % 25)
            poke_list[contador] = [poke_number, x, y]
            return set_pokemon(poke_list, contador + 1, back_ident)


# Funcion que  dibujara los pokemones salvages en pantalla
# Entrada: poke_list es la lista con los datos aleatorios de los pokemones
def draw_wild_poke(poke_list, ventana_principal, wild_sprite_on_world):
    wild_pokemon_1 = ventana_principal.create_image((poke_list[0][1], poke_list[0][2]), image=wild_sprite_on_world,
                                                    tag="wild_poke_1")
    wild_pokemon_2 = ventana_principal.create_image((poke_list[1][1], poke_list[1][2]), image=wild_sprite_on_world,
                                                    tag="wild_poke_2")

    wild_pokemon_3 = ventana_principal.create_image((poke_list[2][1], poke_list[2][2]), image=wild_sprite_on_world,
                                                    tag="wild_poke_3")
    wild_pokemon_4 = ventana_principal.create_image((poke_list[3][1], poke_list[3][2]), image=wild_sprite_on_world,
                                                    tag="wild_poke_4")
    wild_pokemon_5 = ventana_principal.create_image((poke_list[4][1], poke_list[4][2]), image=wild_sprite_on_world,
                                                    tag="wild_poke_5")

# Borra de pantalla y de la lista el pokemon que ha sido capturado o derrotado
# Entradas: poke_list lista de pokemones aleatoria, poke_captured numero de la lista que representa el pokemon capturado
# o derrotado
def delete_captured_poke(poke_list, poke_captured, ventana_principal):
    ventana_principal.delete("wild_poke_" + str(poke_captured))
    poke_list[poke_captured - 1] = [0, -10, -10]


# Borra de la pantalla todos los pokemones salvages generados
def delete_wild_pokemon(ventana_principal):
    ventana_principal.delete("wild_poke_1")
    ventana_principal.delete("wild_poke_2")
    ventana_principal.delete("wild_poke_3")
    ventana_principal.delete("wild_poke_4")
    ventana_principal.delete("wild_poke_5")






