from tkinter import *

# Esta funcion se utiliza para cambiar el fondo
# Entrada:el string que indica el nuevo background
def cambiar_background(back_ident_local, ventana_principal, background_sprite, background):
    # actualiza el valor de back ident a nivel global
    back_ident = back_ident_local
    # verifica el background deseado para cada pantalla para obtener la imagen
    # la cual se asigna a background_sprite
    if back_ident_local == "prof_intro_1":
        background_sprite = PhotoImage(file="sprites/ecenarios/prof_oak_intro_1.png")
    elif back_ident_local == "prof_intro_2":
        background_sprite = PhotoImage(file="sprites/ecenarios/prof_oak_intro_2.png")
    elif back_ident_local == "prof_intro_3":
        background_sprite = PhotoImage(file="sprites/ecenarios/prof_oak_intro_3.png")
    elif back_ident_local == "prof_intro_4":
        background_sprite = PhotoImage(file="sprites/ecenarios/prof_oak_intro_4.png")
    elif back_ident_local == "prof_lab":
        background_sprite = PhotoImage(file="sprites/ecenarios/Prof_Oak_Lab.png")
    elif back_ident_local == "pallet_town":
        background_sprite = PhotoImage(file="sprites/ecenarios/pallet_town.png")
    elif back_ident_local == "ver_for_1":
        background_sprite = PhotoImage(file="sprites/ecenarios/Veridian_Forest_1.png")
    elif back_ident_local == "ver_for_2":
        background_sprite = PhotoImage(file="sprites/ecenarios/Veridian_Forest_2.png")
    elif back_ident_local == "ver_for_3":
        background_sprite = PhotoImage(file="sprites/ecenarios/Veridian_Forest_3.png")
    elif back_ident_local == "ver_for_4":
        background_sprite = PhotoImage(file="sprites/ecenarios/Veridian_Forest_4.png")
    elif back_ident_local == "ver_for_5":
        background_sprite = PhotoImage(file="sprites/ecenarios/Veridian_Forest_5.png")
    elif back_ident_local == "battle_1":
        background_sprite = PhotoImage(file="sprites/ecenarios/batalla_1.png")
    elif back_ident_local == "battle_2":
        background_sprite = PhotoImage(file="sprites/ecenarios/batalla_2.png")
    elif back_ident_local == "choose":
        background_sprite = PhotoImage(file="sprites/ecenarios/choose_pokemon.png")
    elif back_ident_local == "silver_town":
        background_sprite = PhotoImage(file="sprites/ecenarios/silver_town_0.png")
    elif back_ident_local == "silver_town_1":
        background_sprite = PhotoImage(file="sprites/ecenarios/silver_town_1.png")
    elif back_ident_local == "silver_town_2":
        background_sprite = PhotoImage(file="sprites/ecenarios/silver_town_2.png")
    elif back_ident_local == "silver_town_3":
        background_sprite = PhotoImage(file="sprites/ecenarios/silver_town_3.png")
    elif back_ident_local == "gym":
        background_sprite = PhotoImage(file="sprites/ecenarios/gym.png")
    elif back_ident_local == "inicio_batalla":
        background_sprite = PhotoImage(file="sprites/ecenarios/inicio_batalla.png")
    elif back_ident_local == "ganador":
        background_sprite = PhotoImage(file="sprites/ecenarios/ganador.png")
    else:
        pass
    # actualiza el fondo con el background requerida
    label = Label(image=background_sprite)
    label.image = background_sprite
    ventana_principal.itemconfig(background, image=background_sprite, tag="background")
    return back_ident_local



